﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

using Seven.Core.IRepositories;
using Seven.MsSql.Repositories;
using Seven.Entity;
using Seven.EntityBasic;
using Seven.DataModel.RepositoryModels.StaffAbout;
using Seven.DataModel.ViewModels.StaffAbout;
using Seven.Tools.Extension;
using Seven.Tools.Helper;

namespace Seven.Service.Powerful
{
    public partial class HrStaffService
    {
        #region 获取ViewModel

        /// <summary>
        /// 获取新的员工模型
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="deptId">部门ID</param>
        /// <param name="deptName">部门名称</param>
        /// <returns></returns>
        public StaffModel GetNewStaffModel(int companyId, int deptId, string deptName)
        {
            var model = new StaffModel();
            model.CompanyID = companyId;
            model.CompanyName = companyId == 0 ? "" : ServiceFactory.HrOrganization.GetByKey(companyId).Name;
            model.DeptID = deptId;
            model.DeptName = deptName;
            model.Sex = Sex.男;

            return model;
        }

        /// <summary>
        /// 根据员工ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">员工ID</param>
        /// <returns></returns>
        public StaffModel GetEditStaffModel(int id)
        {
            using (var unit = DbFactory.UnitOfWork)
            {
                return unit.HrStaff.GetEditModelByID(id);
            }
        }

        #endregion

        #region 提供json数据

        /// <summary>
        /// 获取员工列表数据
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="deptId">部门ID</param>
        /// <param name="positionId">岗位ID</param>
        /// <param name="model">序列化的查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <returns></returns>
        public JsonResult StaffGrid(int? companyId, int? deptId, int? positionId, string model, int page, int rows)
        {
            using (var unit = DbFactory.UnitOfWork)
            {
                int total = 0;
                var data = unit.HrStaff.StaffGrid(companyId, deptId, positionId, model, page, rows, out total);
                return new DataResult(data).SerializeToGridJsonResult(total, true);
            }
        }

        /// <summary>
        /// 获取未创建用户的员工列表数据
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="deptId">部门ID</param>
        /// <param name="positionId">岗位ID</param>
        /// <param name="realName">员工名字</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <param name="rangeFilter">是否根据管理范围过滤</param>
        /// <returns></returns>
        public JsonResult StaffGridForSelector(int? companyId, int? deptId, int? positionId, string realName, int page, int rows, bool rangeFilter)
        {
            using (var unit = DbFactory.UnitOfWork)
            {
                int total = 0;
                var data = unit.HrStaff.StaffGridWithoutUserRelation(companyId, deptId, positionId, realName, page, rows, out total);
                return new DataResult(data).SerializeToGridJsonResult(total, true);
            }
        }

        /// <summary>
        /// 根据员工主键获取员工列表数据
        /// </summary>
        /// <param name="ids">主键字符串，以英文逗号相连</param>
        /// <returns></returns>
        public JsonResult StaffGrid(string ids)
        {
            using (var unit = DbFactory.UnitOfWork)
            {
                var data = unit.HrStaff.StaffGrid(ids);
                return new DataResult(data).SerializeToGridJsonResult(0, true);
            }
        }

        /// <summary>
        /// 提供性别json数据
        /// </summary>
        /// <returns></returns>
        public JsonResult SexJson()
        {
            var Types = EnumHelper.GetEnumItems<Sex>(typeof(Sex));

            return new DataResult(Types.Select(s => new { s.Value, s.Text })).SerializeToJsonResult();
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 保存员工
        /// </summary>
        /// <param name="model">员工数据模型</param>
        /// <returns>操作结果</returns>
        public JsonResult SaveStaff(StaffModel model)
        {
            if (model.ID == 0)
            {
                HrStaff staff = model.CopyTo<HrStaff>();
                return this.Insert(staff).SerializeToJsonResult();
            }
            else
            {
                HrStaff staff = this.GetByKey(model.ID);

                staff.RealName = model.RealName;
                staff.Sex = model.Sex;
                staff.CompanyID = model.CompanyID;
                staff.DeptID = model.DeptID;
                staff.Remark = model.Remark;

                return this.Update(staff).SerializeToJsonResult();
            }
        }

        /// <summary>
        /// 移除员工
        /// </summary>
        /// <param name="id">员工id</param>
        /// <returns></returns>
        public JsonResult RemoveStaff(int id)
        {
            HrStaff staff;
            using (var unit = DbFactory.UnitOfWork)
            {
                staff = unit.HrStaff.Find(id); if (staff == null) { return new OperationResult(false, true, "员工不存在！").SerializeToJsonResult(); }
                if (unit.SysUser.UseableUserExistByStaffID(staff.ID)) { return new OperationResult(false, "该员工存在对应的用户，无法删除！").SerializeToJsonResult(); }

                staff.IsDeleted = true;
                staff.DeletedDate = DateTime.Now;
                staff.Deleteder = CurrentUserBaseInfo.UserName;

                try
                {
                    unit.BeginTransaction();
                    unit.HrStaff.EntityModified(staff);
                    var msg = new string[] { "删除成功!", "删除失败!" };
                    return new OperationResult(unit.Commit() > 0, msg).SerializeToJsonResult();
                }
                catch (DataAccessException ex)
                {
                    unit.Rollback();
                    throw ExceptionHelper.ThrowServiceException(ex.Message);
                }
            }
        }

        #endregion
    }
}

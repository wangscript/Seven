﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.EntityBasic;

namespace Seven.Service.Security
{
    /// <summary>
    /// 登录基本信息模型
    /// </summary>
    public class UserInfoModel
    {
        /// <summary>
        /// 身份令牌标识
        /// </summary>
        public Guid Token { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 登录使用的账号（用户名/别名）
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 密码密文
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleID { get; set; }

        /// <summary>
        /// 所属集团ID
        /// </summary>
        public int GroupID { get; set; }

        /// <summary>
        /// 所属公司ID
        /// </summary>
        public int CompanyID { get; set; }

        /// <summary>
        /// 所属部门ID
        /// </summary>
        public int DeptID { get; set; }

        /// <summary>
        /// IP地址
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// 是否记住账号
        /// </summary>
        public bool RememberLoginName { get; set; }
    }
}

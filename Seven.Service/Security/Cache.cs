﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Seven.Service.Security
{
    public class Cache
    {
        /// <summary>
        /// 添加“登录名”cookie
        /// </summary>
        /// <param name="value">登录名</param>
        /// <param name="expires">过期时间</param>
        public void AddLoginNameCookie(string value, DateTime expires)
        {
            HttpCookie LoginNameCookie = new HttpCookie(SiteCore.LoginNameCookieName, new Encrypt().Encode(value));
            LoginNameCookie.Expires = expires;
            HttpContext.Current.Response.Cookies.Set(LoginNameCookie);
        }

        /// <summary>
        /// 移除“登录名”cookie
        /// </summary>
        /// <param name="userName">用户名</param>
        public void RemoveLoginNameCookie(string userName)
        {
            HttpCookie LoginNameCookie = HttpContext.Current.Request.Cookies[SiteCore.LoginNameCookieName];
            if (LoginNameCookie != null)
            {
                LoginNameCookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.AppendCookie(LoginNameCookie); //这句一定要加上，否则无法删除
            }
        }

        /// <summary>
        /// 添加“主题皮肤”cookie
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="value">主题皮肤名</param>
        public void AddThemeCookie(string userName, string value)
        {
            HttpCookie ThemeCookie = new HttpCookie(SiteCore.ThemeCookieNamePrefix + userName, value);
            ThemeCookie.Expires = DateTime.Now.AddDays(1);
            HttpContext.Current.Response.Cookies.Set(ThemeCookie);
        }

        /// <summary>
        /// 移除“主题皮肤”cookie
        /// </summary>
        /// <param name="userName">用户名</param>
        public void RemoveThemeCookie(string userName)
        {
            HttpCookie ThemeCookie = HttpContext.Current.Request.Cookies[SiteCore.ThemeCookieNamePrefix + userName];
            if (ThemeCookie != null)
            {
                ThemeCookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.AppendCookie(ThemeCookie); //这句一定要加上，否则无法删除
            }
        }

        /// <summary>
        /// 添加“组织结构范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="value">组织结构范围</param>
        public void AddOrganRangeSession(string userName, IEnumerable<Seven.Entity.HrOrganization> value)
        {
            string name = SiteCore.OrganRangeCookieNamePrefix + userName;
            var Range = HttpContext.Current.Session[name];
            if (Range == null)
            {
                HttpContext.Current.Session.Add(name, value);
            }
            else
            {
                HttpContext.Current.Session[name] = value;
            }
        }

        /// <summary>
        /// 移除“组织结构范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        public void RemoveOrganRangeSession(string userName)
        {
            HttpContext.Current.Session.Remove(SiteCore.MenuFunctionRangeCookieNamePrefix + userName);
        }

        /// <summary>
        /// 获取“组织结构范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public object GetOrganRangeSession(string userName)
        {
            return HttpContext.Current.Session[SiteCore.OrganRangeCookieNamePrefix + userName];
        }

        /// <summary>
        /// 添加“菜单权限范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="value">菜单权限范围</param>
        public void AddMenuPermissionRangeSession(string userName, IEnumerable<Seven.Entity.SysMenuPermission> value)
        {
            string name = SiteCore.MenuPermissionRangeCookieNamePrefix + userName;
            var Range = HttpContext.Current.Session[name];
            if (Range == null)
            {
                HttpContext.Current.Session.Add(name, value);
            }
            else
            {
                HttpContext.Current.Session[name] = value;
            }
        }

        /// <summary>
        /// 移除“菜单权限范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        public void RemoveMenuPermissionRangeSession(string userName)
        {
            HttpContext.Current.Session.Remove(SiteCore.MenuPermissionRangeCookieNamePrefix + userName);
        }

        /// <summary>
        /// 获取“菜单权限范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public object GetMenuPermissionRangeSession(string userName)
        {
            return HttpContext.Current.Session[SiteCore.MenuPermissionRangeCookieNamePrefix + userName];
        }

        /// <summary>
        /// 添加“菜单按钮权限范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="value">菜单按钮权限范围</param>
        public void AddMenuButtonPermissionRangeSession(string userName, IEnumerable<Seven.Service.Security.ButtonPermissionModel> value)
        {
            string name = SiteCore.MenuButtonPermissionRangeCookieNamePrefix + userName;
            var Range = HttpContext.Current.Session[name];
            if (Range == null)
            {
                HttpContext.Current.Session.Add(name, value);
            }
            else
            {
                HttpContext.Current.Session[name] = value;
            }
        }

        /// <summary>
        /// 移除“菜单按钮权限范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        public void RemoveMenuButtonPermissionRangeSession(string userName)
        {
            HttpContext.Current.Session.Remove(SiteCore.MenuButtonPermissionRangeCookieNamePrefix + userName);
        }

        /// <summary>
        /// 获取“菜单按钮权限范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public object GetMenuButtonPermissionRangeSession(string userName)
        {
            return HttpContext.Current.Session[SiteCore.MenuButtonPermissionRangeCookieNamePrefix + userName];
        }

        /// <summary>
        /// 添加“菜单功能点范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="value">菜单功能点范围</param>
        public void AddMenuFunctionRangeSession(string userName, IEnumerable<Seven.Entity.SysMenuFunction> value)
        {
            string name = SiteCore.MenuFunctionRangeCookieNamePrefix + userName;
            var Range = HttpContext.Current.Session[name];
            if (Range == null)
            {
                HttpContext.Current.Session.Add(name, value);
            }
            else
            {
                HttpContext.Current.Session[name] = value;
            }
        }

        /// <summary>
        /// 移除“菜单功能点范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        public void RemoveMenuFunctionRangeSession(string userName)
        {
            HttpContext.Current.Session.Remove(SiteCore.MenuFunctionRangeCookieNamePrefix + userName);
        }

        /// <summary>
        /// 获取“菜单功能点范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public object GetMenuFunctionRangeSession(string userName)
        {
            return HttpContext.Current.Session[SiteCore.MenuFunctionRangeCookieNamePrefix + userName];
        }

        /// <summary>
        /// 添加“菜单范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="value">菜单范围</param>
        public void AddMenuRangeSession(string userName, IEnumerable<Seven.Entity.SysMenu> value)
        {
            string name = SiteCore.MenuRangeCookieNamePrefix + userName;
            var Range = HttpContext.Current.Session[name];
            if (Range == null)
            {
                HttpContext.Current.Session.Add(name, value);
            }
            else
            {
                HttpContext.Current.Session[name] = value;
            }
        }

        /// <summary>
        /// 移除“菜单范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        public void RemoveMenuRangeSession(string userName)
        {
            HttpContext.Current.Session.Remove(SiteCore.MenuRangeCookieNamePrefix + userName);
        }

        /// <summary>
        /// 获取“菜单范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public object GetMenuRangeSession(string userName)
        {
            return HttpContext.Current.Session[SiteCore.MenuRangeCookieNamePrefix + userName];
        }
    }
}

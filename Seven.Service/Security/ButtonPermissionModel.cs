﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Service.Security
{
    public class ButtonPermissionModel
    {
        /// <summary>
        /// 权限所在菜单ID
        /// </summary>
        public int MenuID { get; set; }

        /// <summary>
        /// 分组标记
        /// </summary>
        public string GroupMark { get; set; }

        /// <summary>
        /// 按钮名称
        /// </summary>
        public string ButtonName { get; set; }

        /// <summary>
        /// 按钮图标
        /// </summary>
        public string IconCls { get; set; }

        /// <summary>
        /// 按钮点击事件
        /// </summary>
        public string HandlerName { get; set; }

        /// <summary>
        /// 是否禁用
        /// </summary>
        public bool IsDisabled { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SortNumber { get; set; }
    }
}

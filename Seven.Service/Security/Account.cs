﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

using Seven.Entity;
using Seven.DataModel.ViewModels.UserAbout;
using Seven.Service.Powerful;
using Seven.Tools.Helper;
using Seven.Tools.Extension;

namespace Seven.Service.Security
{
    public class Account
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="model">登录模型信息</param>
        /// <returns>业务操作结果</returns>
        public OperationResult Login(LoginModel model)
        {
            if (string.IsNullOrWhiteSpace(model.UserName)) { return new OperationResult(false, "登录名不能为空！"); }
            if (string.IsNullOrWhiteSpace(model.Password)) { return new OperationResult(false, "密码不能为空！"); }

            if (SiteCore.UseRemotePool)
            {
                #region 通过远程用户池进行登录

                return this.LoginByMemberPool(model);

                #endregion
            }
            else
            {
                #region 本地登录

                return this.LoginLocal(model);

                #endregion
            }
        }

        /// <summary>
        /// 本地登录
        /// </summary>
        /// <param name="model">登录模型信息</param>
        /// <returns></returns>
        private OperationResult LoginLocal(LoginModel model)
        {
            SysUserService US = ServiceFactory.SysUser;
            OperationResult result = US.CheckLogin(model.UserName, model.Password);
            if (result.Success)
            {
                SysUser userData = (SysUser)result.Data;
                UserInfoModel LoginInfo = new UserInfoModel(); Cache C = new Cache();

                #region 将额外信息赋值给用户数据模型

                LoginInfo.UserID = userData.ID;
                LoginInfo.UserName = userData.UserName;
                LoginInfo.Account = model.UserName;
                LoginInfo.Password = userData.PassWord;
                LoginInfo.RealName = userData.Staff.RealName;
                LoginInfo.RoleID = userData.RoleID;
                LoginInfo.GroupID = userData.Staff.GroupID;
                LoginInfo.CompanyID = userData.Staff.CompanyID;
                LoginInfo.DeptID = userData.Staff.DeptID;
                LoginInfo.IpAddress = HttpContext.Current.Request.UserHostAddress;
                LoginInfo.RememberLoginName = model.RememberLoginName;

                #endregion

                #region Forms身份验证记录

                string data = JsonHelper.SerializeObjectTransferEnum(LoginInfo);

                DateTime expiration = LoginInfo.RememberLoginName
                    ? DateTime.Now.AddDays(7)
                    : DateTime.Now.Add(FormsAuthentication.Timeout);
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, LoginInfo.Account, DateTime.Now, expiration,
                    true, data, FormsAuthentication.FormsCookiePath);
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
                cookie.Expires = ticket.Expiration;
                HttpContext.Current.Response.Cookies.Set(cookie);

                #endregion

                #region 记住账号

                if (LoginInfo.RememberLoginName)
                {
                    C.AddLoginNameCookie(LoginInfo.Account, DateTime.Now.AddDays(7));
                }
                else
                {
                    C.RemoveLoginNameCookie(LoginInfo.UserName);
                }

                #endregion

                #region 缓存范围

                HrOrganizationService OS = ServiceFactory.HrOrganization; 
                SysMenuFunctionService MFS = ServiceFactory.SysMenuFunction; 
                SysMenuService MS = ServiceFactory.SysMenu; 
                SysMenuPermissionService MPS = ServiceFactory.SysMenuPermission; 
                SysUserMenuPermissionService UMPS = ServiceFactory.SysUserMenuPermission;

                //组织结构范围
                IEnumerable<HrOrganization> organsRange = OS.GetRangeOrgans(LoginInfo.UserID);
                C.AddOrganRangeSession(LoginInfo.UserName, organsRange);
                //菜单权限范围
                //用户可用的的菜单权限
                IEnumerable<SysMenuPermission> menuPermissionRange = MPS.GetRangeMenuPermissions(LoginInfo.UserID, LoginInfo.UserName.Equals(SiteCore.DefaultSuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase));
                C.AddMenuPermissionRangeSession(LoginInfo.UserName, menuPermissionRange);
                //所有的菜单按钮权限数据集合
                IEnumerable<ButtonPermissionModel> menuButtonPermission = MPS.GetButtonPermissions(menuPermissionRange.Where(w => w.Type == EntityBasic.PermissionType.按钮));
                C.AddMenuButtonPermissionRangeSession(LoginInfo.UserName, menuButtonPermission);
                //菜单功能点范围
                IEnumerable<SysMenuFunction> menuFunctionsRange = MFS.GetRangeMenuFunctions(menuPermissionRange.Select(s => s.FunctionIDs), true);
                C.AddMenuFunctionRangeSession(LoginInfo.UserName, menuFunctionsRange);
                //菜单范围
                IEnumerable<SysMenu> menuRange = MS.GetRangeMenus(menuFunctionsRange);
                C.AddMenuRangeSession(LoginInfo.UserName, menuRange);

                #endregion
            }
            return result;
        }

        /// <summary>
        /// 通过远程用户池进行登录
        /// </summary>
        /// <param name="model">登录模型信息</param>
        /// <returns></returns>
        private OperationResult LoginByMemberPool(LoginModel model)
        {
            UserInfoModel LoginInfo = null;
            OperationResult Result = ServicePool.NormalLogin(model, ref LoginInfo);
            if (Result.Success)
            {
                Cache C = new Cache(); SysUserService US = ServiceFactory.SysUser;

                #region 将额外信息赋值给用户数据模型

                LoginInfo.IpAddress = HttpContext.Current.Request.UserHostAddress;
                LoginInfo.RememberLoginName = model.RememberLoginName;

                #endregion

                #region Forms身份验证记录

                string data = JsonHelper.SerializeObjectTransferEnum(LoginInfo);

                DateTime expiration = LoginInfo.RememberLoginName
                    ? DateTime.Now.AddDays(7)
                    : DateTime.Now.Add(FormsAuthentication.Timeout);
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, LoginInfo.Account, DateTime.Now, expiration,
                    true, data, FormsAuthentication.FormsCookiePath);
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
                cookie.Expires = ticket.Expiration;
                HttpContext.Current.Response.Cookies.Set(cookie);

                #endregion

                #region 记住账号

                if (LoginInfo.RememberLoginName)
                {
                    C.AddLoginNameCookie(LoginInfo.Account, DateTime.Now.AddDays(7));
                }
                else
                {
                    C.RemoveLoginNameCookie(LoginInfo.UserName);
                }

                #endregion

                #region 缓存范围

                HrOrganizationService OS = ServiceFactory.HrOrganization; 
                SysMenuFunctionService MFS = ServiceFactory.SysMenuFunction; 
                SysMenuService MS = ServiceFactory.SysMenu; 
                SysMenuPermissionService MPS = ServiceFactory.SysMenuPermission; 
                SysUserMenuPermissionService UMPS = ServiceFactory.SysUserMenuPermission;

                //组织结构范围
                IEnumerable<HrOrganization> organsRange = OS.GetRangeOrgans(LoginInfo.UserID);
                C.AddOrganRangeSession(LoginInfo.UserName, organsRange);
                //菜单权限范围
                //用户可用的的菜单权限
                IEnumerable<SysMenuPermission> menuPermissionRange = MPS.GetRangeMenuPermissions(LoginInfo.UserID, LoginInfo.UserName.Equals(SiteCore.DefaultSuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase));
                C.AddMenuPermissionRangeSession(LoginInfo.UserName, menuPermissionRange);
                //所有的菜单按钮权限数据集合
                IEnumerable<ButtonPermissionModel> menuButtonPermission = MPS.GetButtonPermissions(menuPermissionRange.Where(w => w.Type == EntityBasic.PermissionType.按钮));
                C.AddMenuButtonPermissionRangeSession(LoginInfo.UserName, menuButtonPermission);
                //菜单功能点范围
                IEnumerable<SysMenuFunction> menuFunctionsRange = MFS.GetRangeMenuFunctions(menuPermissionRange.Select(s => s.FunctionIDs), true);
                C.AddMenuFunctionRangeSession(LoginInfo.UserName, menuFunctionsRange);
                //菜单范围
                IEnumerable<SysMenu> menuRange = MS.GetRangeMenus(menuFunctionsRange);
                C.AddMenuRangeSession(LoginInfo.UserName, menuRange);

                #endregion
            }
            return Result;
        }

        /// <summary>
        /// 获取登录用户的基本信息
        /// </summary>
        /// <returns></returns>
        public UserInfoModel GetCurrentUserBaseInfo()
        {
            HttpContext context = HttpContext.Current; UserInfoModel UserInfo = null;

            #region 从Forms验证存储的票据中获取用户的基本信息

            // 1. 读登录Cookie
            HttpCookie cookie = context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
            {
                // 2. 解密Cookie值，获取FormsAuthenticationTicket对象
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
                if (ticket != null && !string.IsNullOrEmpty(ticket.UserData))
                {
                    // 3. 还原用户数据
                    UserInfo = JsonHelper.DeserializeObject<UserInfoModel>(ticket.UserData);
                }
            }

            #endregion

            return UserInfo;
        }

        /// <summary>
        /// 获取登录用户的已登录数据模型（含身份令牌标识，用来调用WebApi）
        /// </summary>
        /// <returns></returns>
        public LoginedModel GetCurrentUserLoginedModel()
        {
            UserInfoModel info = this.GetCurrentUserBaseInfo();
            return new LoginedModel { Token = info.Token, Account = info.Account };
        }

        #region 获取缓存中的组织结构范围

        /// <summary>
        /// 根据用户信息获取当前用户缓存中的组织结构范围
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public IEnumerable<HrOrganization> GetCurrentUserOrganRange(int userId, string userName)
        {
            Cache C = new Cache();
            object range = C.GetOrganRangeSession(userName);
            if (range == null)
            {
                IEnumerable<HrOrganization> organsRange = ServiceFactory.HrOrganization.GetRangeOrgans(userId);
                C.AddOrganRangeSession(userName, organsRange);
                return organsRange;
            }
            else
            {
                return (IEnumerable<HrOrganization>)range;
            }
        }

        /// <summary>
        /// 根据用户信息获取当前用户缓存中的组织结构范围
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public IEnumerable<HrOrganization> GetCurrentUserOrganRange(string userName)
        {
            Cache C = new Cache();
            object range = C.GetOrganRangeSession(userName);
            if (range == null)
            {
                int userId = this.GetCurrentUserBaseInfo().UserID;
                IEnumerable<HrOrganization> organsRange = ServiceFactory.HrOrganization.GetRangeOrgans(userId);
                C.AddOrganRangeSession(userName, organsRange);
                return organsRange;
            }
            else
            {
                return (IEnumerable<HrOrganization>)range;
            }
        }

        /// <summary>
        /// 获取当前用户缓存中的组织结构范围
        /// </summary>
        /// <returns></returns>
        public IEnumerable<HrOrganization> GetCurrentUserOrganRange()
        {
            string userName = GetCurrentUserBaseInfo().UserName;
            return this.GetCurrentUserOrganRange(userName);
        }

        #endregion

        #region 获取缓存中的菜单权限范围

        /// <summary>
        /// 根据用户信息获取当前用户缓存中的菜单按钮权限范围
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public IEnumerable<SysMenuPermission> GetCurrentUserMenuPermissionRange(int userId, string userName)
        {
            Cache C = new Cache();
            object range = C.GetMenuPermissionRangeSession(userName);
            if (range == null)
            {
                bool all = userName.Equals(SiteCore.DefaultSuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                IEnumerable<SysMenuPermission> menuPermissionRange = ServiceFactory.SysMenuPermission.GetRangeMenuPermissions(userId, all);
                C.AddMenuPermissionRangeSession(userName, menuPermissionRange);
                return menuPermissionRange;
            }
            else
            {
                return (IEnumerable<SysMenuPermission>)range;
            }
        }

        /// <summary>
        /// 根据用户信息获取当前用户缓存中的菜单权限范围
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public IEnumerable<SysMenuPermission> GetCurrentUserMenuPermissionRange(string userName)
        {
            Cache C = new Cache();
            object range = C.GetMenuPermissionRangeSession(userName);
            if (range == null)
            {
                int userId = this.GetCurrentUserBaseInfo().UserID;
                bool all = userName.Equals(SiteCore.DefaultSuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                IEnumerable<SysMenuPermission> menuPermissionRange = ServiceFactory.SysMenuPermission.GetRangeMenuPermissions(userId, all);
                C.AddMenuPermissionRangeSession(userName, menuPermissionRange);
                return menuPermissionRange;
            }
            else
            {
                return (IEnumerable<SysMenuPermission>)range;
            }
        }

        /// <summary>
        /// 获取当前用户缓存中的菜单权限范围
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SysMenuPermission> GetCurrentUserMenuPermissionRange()
        {
            string userName = this.GetCurrentUserBaseInfo().UserName;
            return this.GetCurrentUserMenuPermissionRange(userName);
        }

        #endregion

        #region 获取缓存中的菜单按钮权限范围

        /// <summary>
        /// 根据用户信息获取当前用户缓存中的菜单按钮权限范围
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public IEnumerable<ButtonPermissionModel> GetCurrentUserMenuButtonPermissionRange(int userId, string userName)
        {
            Cache C = new Cache();
            object range = C.GetMenuButtonPermissionRangeSession(userName);
            if (range == null)
            {
                bool all = userName.Equals(SiteCore.DefaultSuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                IEnumerable<ButtonPermissionModel> menuButtonPermissionsRange = ServiceFactory.SysMenuPermission.GetButtonPermissions(userId, all);
                C.AddMenuButtonPermissionRangeSession(userName, menuButtonPermissionsRange);
                return menuButtonPermissionsRange;
            }
            else
            {
                return (IEnumerable<ButtonPermissionModel>)range;
            }
        }

        /// <summary>
        /// 根据用户信息获取当前用户缓存中的菜单按钮权限范围
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public IEnumerable<ButtonPermissionModel> GetCurrentUserMenuButtonPermissionRange(string userName)
        {
            Cache C = new Cache();
            object range = C.GetMenuButtonPermissionRangeSession(userName);
            if (range == null)
            {
                int userId = this.GetCurrentUserBaseInfo().UserID;
                bool all = userName.Equals(SiteCore.DefaultSuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                IEnumerable<ButtonPermissionModel> menuButtonPermissionsRange = ServiceFactory.SysMenuPermission.GetButtonPermissions(userId, all);
                C.AddMenuButtonPermissionRangeSession(userName, menuButtonPermissionsRange);
                return menuButtonPermissionsRange;
            }
            else
            {
                return (IEnumerable<ButtonPermissionModel>)range;
            }
        }

        /// <summary>
        /// 获取当前用户缓存中的菜单按钮权限范围
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ButtonPermissionModel> GetCurrentUserMenuButtonPermissionRange()
        {
            string userName = this.GetCurrentUserBaseInfo().UserName;
            return this.GetCurrentUserMenuButtonPermissionRange(userName);
        }

        #endregion

        #region 获取缓存中的菜单功能点范围

        /// <summary>
        /// 根据用户信息获取当前用户缓存中的菜单功能点范围，附带菜单导航属性的数据
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public IEnumerable<SysMenuFunction> GetCurrentUserMenuFunctionRange(int userId, string userName)
        {
            Cache C = new Cache();
            object range = C.GetMenuFunctionRangeSession(userName);
            if (range == null)
            {
                bool all = userName.Equals(SiteCore.DefaultSuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                IEnumerable<SysMenuFunction> menuFunctionsRange = ServiceFactory.SysMenuFunction.GetRangeMenuFunctions(userId, all, true);
                C.AddMenuFunctionRangeSession(userName, menuFunctionsRange);
                return menuFunctionsRange;
            }
            else
            {
                return (IEnumerable<SysMenuFunction>)range;
            }
        }

        /// <summary>
        /// 根据用户信息获取当前用户缓存中的菜单功能点范围，附带菜单导航属性的数据
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public IEnumerable<SysMenuFunction> GetCurrentUserMenuFunctionRange(string userName)
        {
            Cache C = new Cache();
            object range = C.GetMenuFunctionRangeSession(userName);
            IEnumerable<SysMenuFunction> menuFunctionsRange;
            if (range == null)
            {
                int userId = this.GetCurrentUserBaseInfo().UserID;
                bool all = userName.Equals(SiteCore.DefaultSuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                menuFunctionsRange = ServiceFactory.SysMenuFunction.GetRangeMenuFunctions(userId, all, true);
                C.AddMenuFunctionRangeSession(userName, menuFunctionsRange);
            }
            else
            {
                menuFunctionsRange = (IEnumerable<SysMenuFunction>)range;
            }

            return menuFunctionsRange;
        }

        /// <summary>
        /// 获取当前用户缓存中的菜单功能点范围，附带菜单导航属性的数据
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SysMenuFunction> GetCurrentUserMenuFunctionRange()
        {
            string userName = GetCurrentUserBaseInfo().UserName;
            return this.GetCurrentUserMenuFunctionRange(userName);
        }

        #endregion

        #region 获取缓存中的菜单范围

        /// <summary>
        /// 根据用户信息获取当前用户缓存中的菜单范围
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public IEnumerable<SysMenu> GetCurrentUserMenuRange(int userId, string userName)
        {
            Cache C = new Cache();
            object Range1 = C.GetMenuRangeSession(userName);
            if (Range1 == null)
            {
                IEnumerable<SysMenu> MenuRange = ServiceFactory.SysMenu.GetRangeMenus(userId, userName.Equals(SiteCore.DefaultSuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase));
                C.AddMenuRangeSession(userName, MenuRange);
                return MenuRange;
            }
            else
            {
                return (IEnumerable<SysMenu>)Range1;
            }
        }

        /// <summary>
        /// 根据用户信息获取当前用户缓存中的菜单范围
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public IEnumerable<SysMenu> GetCurrentUserMenuRange(string userName)
        {
            Cache C = new Cache();
            object Range1 = C.GetMenuRangeSession(userName);
            if (Range1 == null)
            {
                int userId = this.GetCurrentUserBaseInfo().UserID;
                bool all = userName.Equals(SiteCore.DefaultSuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                IEnumerable<SysMenu> MenuRange = ServiceFactory.SysMenu.GetRangeMenus(userId, all);
                C.AddMenuRangeSession(userName, MenuRange);
                return MenuRange;
            }
            else
            {
                return (IEnumerable<SysMenu>)Range1;
            }
        }

        /// <summary>
        /// 获取当前用户缓存中的菜单范围
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SysMenu> GetCurrentUserMenuRange()
        {
            string userName = GetCurrentUserBaseInfo().UserName;
            return this.GetCurrentUserMenuRange(userName);
        }

        #endregion

        /// <summary>
        /// 获取当前用户的按钮权限数据，返回json格式的字符串
        /// </summary>
        /// <param name="controller">控制器名</param>
        /// <param name="action">动作名</param>
        /// <param name="groupMarks">分组标记，表示只获取指定分组标记的按钮权限数据。不区分大小写。</param>
        /// <returns></returns>
        public string GetCurrentUserButtons(string controller, string action, IEnumerable<string> groupMarks)
        {
            UserInfoModel info = this.GetCurrentUserBaseInfo();
            IEnumerable<ButtonPermissionModel> permissions = this.GetCurrentUserMenuButtonPermissionRange(info.UserName);
            IEnumerable<SysMenu> menus = this.GetCurrentUserMenuRange(info.UserName);
            SysMenu menu = menus.FirstOrDefault(f => !string.IsNullOrWhiteSpace(f.ControllerName) && !string.IsNullOrWhiteSpace(f.ActionName) && f.ControllerName.Equals(controller, StringComparison.InvariantCultureIgnoreCase) && f.ActionName.Equals(action, StringComparison.InvariantCultureIgnoreCase));
            int menuId = menu == null ? 0 : menu.ID;
            IEnumerable<ButtonPermissionModel> data = permissions.Where(w => w.MenuID == menuId);
            if (groupMarks.Count() > 0) { data = data.Where(w => !string.IsNullOrWhiteSpace(w.GroupMark) && groupMarks.Contains(w.GroupMark.ToLower())); }
            return Seven.Tools.Helper.JsonHelper.SerializeObject(data.TryOrderBy(o => o.SortNumber).Select(s => new { s.ButtonName, s.IconCls, s.HandlerName, s.GroupMark, s.IsDisabled }));
        }

        /// <summary>
        /// 根据用户ID获取该用户的基本配置信息【无配置信息则返回新实例】
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        public SysUserBaseConfig GetUserBaseConfig(int userId)
        {
            return ServiceFactory.SysUserBaseConfig.GetUserBaseConfig(userId);
        }

        /// <summary>
        /// 保存布局位置
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="region">位置，只能是 north、west、south</param>
        /// <param name="collapse">是否折叠</param>
        public void SaveLayoutState(int userId, string region, bool collapse)
        {
            string temp = region.ToLower();
            if (temp != "north" && temp != "west" && temp != "south") { return; }
            ServiceFactory.SysUserBaseConfig.SaveLayoutState(userId, temp, collapse);
        }

        /// <summary>
        /// 保存用户默认的主菜单ID
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="menuId">主菜单ID</param>
        public void SaveRootMenuID(int userId, int menuId)
        {
            ServiceFactory.SysUserBaseConfig.SaveRootMenuID(userId, menuId);
        }

        /// <summary>
        /// 保存用户默认的主题
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="themeName">主题名称</param>
        public void SaveThemeName(int userId, string themeName)
        {
            ServiceFactory.SysUserBaseConfig.SaveThemeName(userId, themeName);
        }


        /// <summary>
        /// 注销退出
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public OperationResult Logout(string userName)
        {
            OperationResult result;
            if (SiteCore.UseRemotePool)
            {
                result = ServicePool.NormalLogout(this.GetCurrentUserLoginedModel());
            }
            else
            {
                result = new OperationResult(true);
            }
            if (result.Success)
            {
                Cache C = new Cache();
                C.RemoveThemeCookie(userName);
                C.RemoveMenuRangeSession(userName);
                C.RemoveMenuFunctionRangeSession(userName);
                C.RemoveMenuButtonPermissionRangeSession(userName);
                C.RemoveOrganRangeSession(userName);

                FormsAuthentication.SignOut();
            }
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using Seven.Member.Web.Model;

namespace Seven.Member.Web.Filters.Mvc
{
    /// <summary>
    ///  定义一个用于刷新 ASP.NET MVC 登录用户登录身份 Token 状态的过滤器。
    /// </summary>
    public class UpdateLoginTokenStateFilterAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// 在 Action 执行之前操作，刷新用户登录身份的状态。
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            LoginModel model = LoginModelUtility.GetLoginModel(filterContext.HttpContext);
            if (model != null && !string.IsNullOrWhiteSpace(model.Token))
            {
                Guid tokenKey = Guid.Empty;
                if (Guid.TryParse(model.Token, out tokenKey))
                { MemberClient.UpdateTokenLastRequestTime(tokenKey); }

                LoginModelUtility.SetLoginModel(filterContext.HttpContext, model);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Seven.Member
{
    /// <summary>
    /// 表示执行用户登出操作的结果
    /// </summary>
    public enum LogoutResult
    {
        /// <summary>
        /// 表示登出操作时出现了一个未识别的错误。
        /// </summary>
        [Description("登出操作时出现了一个未识别的错误。")]
        Error = 0,

        /// <summary>
        /// 表示登出操作时对页面随机票据号的校验不匹配，非法的登出操作。
        /// </summary>
        [Description("登出操作时对页面随机票据号的校验不匹配，非法的登出操作。")]
        InvalidCaptcha = 3,

        /// <summary>
        /// 表示登出操作时用户的状态校验不通过（尚未登入）。
        /// </summary>
        [Description("登出操作时用户的状态校验不通过（尚未登入）。")]
        InvalidLoginStatus = 4,

        /// <summary>
        /// 表示设备登录时设备的状态校验不通过（该设备尚未登入）。
        /// </summary>
        [Description("设备登录时设备的状态校验不通过（该设备尚未登入）。")]
        InvalidDeviceStatus = 6,

        /// <summary>
        /// 表示登出操作成功。
        /// </summary>
        [Description("登出操作成功。")]
        Success = 200,
    }
}

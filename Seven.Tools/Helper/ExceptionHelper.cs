﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Tools.Extension;

namespace Seven.Tools.Helper
{
    public static class ExceptionHelper
    {
        /// <summary>
        /// 向调用层抛出数据访问层异常
        /// </summary>
        /// <param name="msg"> 自定义异常消息 </param>
        /// <param name="e"> 实际引发异常的异常实例 </param>
        public static DataAccessException ThrowDataAccessException(string msg, Exception e = null)
        {
            string temp = msg;
            if (string.IsNullOrEmpty(msg) && e != null)
            {
                temp = e.Message;
            }
            else if (string.IsNullOrEmpty(msg))
            {
                temp = "未知数据访问层异常，详情请查看日志信息。";
            }
            return e == null
                ? new DataAccessException(string.Format("数据访问层异常：{0}", temp))
                : new DataAccessException(string.Format("数据访问层异常：{0}", temp), e);
        }

        /// <summary>
        /// 向调用层抛出业务逻辑层异常
        /// </summary>
        /// <param name="msg"> 自定义异常消息 </param>
        /// <param name="e"> 实际引发异常的异常实例 </param>
        public static BusinessException ThrowServiceException(string msg, Exception e = null)
        {
            string temp = msg;
            if (string.IsNullOrEmpty(msg) && e != null)
            {
                temp = e.Message;
            }
            else if (string.IsNullOrEmpty(msg))
            {
                temp = "未知业务逻辑层异常，详情请查看日志信息。";
            }
            return e == null
                ? new BusinessException(string.Format("业务逻辑层异常：{0}", temp))
                : new BusinessException(string.Format("业务逻辑层异常：{0}", temp), e);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Tools.Helper
{
    public static class UrlHelper
    {
        /// <summary>
        /// 组装成可用的url
        /// </summary>
        /// <param name="controller">控制器名称</param>
        /// <param name="action">动作名称</param>
        /// <returns></returns>
        public static string GetRealUrl(string controller, string action)
        {
            if (string.IsNullOrWhiteSpace(controller) || string.IsNullOrWhiteSpace(action))
            {
                return "";
            }
            else
            {
                return "/" + controller + "/" + action;
            }
        }
    }
}

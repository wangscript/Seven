﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Seven.Pool.Models;
using Seven.Member;
using Seven.Member.Web.Filters.Http;
using Seven.Web.Http;

namespace Seven.Pool.Controllers
{
    public class MemberServiceController : ApiController
    {
        /// <summary>
        /// 同步对象
        /// </summary>
        private static readonly object sync = new object();

        /// <summary>
        /// 静态字典，用来存储每天的访问量。Key为日期string格式值，如“2015-3-9”，Value为该日对本WebApi控制器的访问量
        /// </summary>
        public static IDictionary<string, int> CountPerDay = new Dictionary<string, int>();

        public MemberServiceController()
        {
            lock (sync)
            {
                string day = DateTime.Now.ToString("yyyy-MM-dd");
                if (CountPerDay.ContainsKey(day))
                {
                    CountPerDay[day]++;
                }
                else
                { CountPerDay.Add(day, 1); }
            }
        }

        /// <summary>
        /// 测试当前webapi访问是否正常
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string Access()
        {
            return "WebApi服务正常！";
        }

        /// <summary>
        /// 常规登录
        /// </summary>
        /// <param name="model">登录数据模型</param>
        /// <returns></returns>
        [HttpPost]
        public LoginResultModel NormalLogin([FromBody]LoginModel model)
        {
            UserInfoModel data;
            var login = Seven.Member.MemberClient.Login(model.Account, model.Password, out data);
            return new LoginResultModel { Success = login == Member.LoginResult.Success, FailMessage = login.GetDescriptionValue(), Data = data };
        }

        /// <summary>
        /// 常规注销退出
        /// 注，特性LoginStatusAuthorize中对request.content中的body参数进行解析，判定其身份是否合法。故调用本方法时，请在content中加上身份信息参数。参数属性是Token、Account、IMEI。
        /// </summary>
        /// <param name="token">身份令牌标识</param>
        /// <returns></returns>
        [LoginStatusAuthorize]
        [HttpPost]
        public ResultModel NormalLogout([FromUri]string token)
        {
            Guid g = Guid.Empty;
            if (Guid.TryParse(token, out g))
            {
                var logout = Seven.Member.MemberClient.Logout(g);
                ResultModel result = new ResultModel { Success = logout == LogoutResult.Success, FailMessage = logout.GetDescriptionValue() };

                return result;
            }
            else { return new ResultModel { Success = false, FailMessage = "提供的身份令牌无效！" }; }
        }

        /// <summary>
        /// 检查用户登录状态是否有效
        /// </summary>
        /// <param name="account">账号</param>
        /// <returns></returns>
        [LoginStatusAuthorize]
        [HttpPost]
        public bool CheckLoginState([FromUri]string account)
        {
            return true;
        }

        /// <summary>
        /// 刷新用户身份标识的最后一次请求到服务器的时间
        /// </summary>
        /// <param name="account">账号</param>
        /// <returns>返回在Member中获得的：请求之间所允许的时间（以分钟为单位）</returns>
        [LoginStatusAuthorize]
        [UpdateLoginTokenStateFilter]
        [HttpPost]
        public int UpdateState([FromUri]string account)
        {
            return Member.Common.Config.SessionTimeout;
        }
    }
}
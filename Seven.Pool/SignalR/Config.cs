﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seven.Pool.SignalR
{
    public class Config
    {
        /// <summary>
        /// SignalR服务池的超管账户
        /// </summary>
        public static readonly string SignalRAdminAccount = "SevenAdmin";

        /// <summary>
        /// SignalR服务池的超管账户的分组名
        /// </summary>
        public static readonly string SignalRAdminGroup = "SignalRService";
    }
}
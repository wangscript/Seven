﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seven.Pool.SignalR
{
    public class MemberClient
    {
        /// <summary>
        /// 服务器超级用户的连接ID
        /// </summary>
        internal static string AdminConnectionID { get; set; }

        #region 内部属性定义 - 获取连接用户信息

        /// <summary>
        /// 获取用于存储连接用户列表信息的集合。
        /// </summary>
        internal static MemberTokenCollection Tokens
        {
            get { return MemberToken.Tokens; }
        }

        #endregion

        #region 公共属性定义 - 获取连接用户信息

        /// <summary>
        /// 获取当前连接的所有身份信息列表。
        /// <para>注意该属性每次进行 get 读取操作时都会生成一个新的数组，所以如果要在一个会话/方法中多次读取该结果集，建议在一次获取该属性值后用变量缓存起来供多次调用。</para>
        /// </summary>
        public static MemberToken[] AllTokens
        {
            get { return Tokens.GetAllToken(); }
        }

        /// <summary>
        /// 获取当前连接的所有用户的总数量。
        /// <para>该属性每次进行 get 操作时都会对连接用户列表进行一次数量统计，所以如果要在一个会话/方法中多次读取该属性值，建议在一次获取该属性值后用变量缓存起来供多次调用。</para>
        /// </summary>
        public static int AllTokensCount
        {
            get { return AllTokens.Length; }
        }

        #endregion

        #region 公共方法定义 - 添加、移除连接

        /// <summary>
        /// 连接服务的核心 API 之一；以指定的用户名、用户池token标识、分组名和SignalRID进行添加连接操作。
        /// <para>若指定的用户池token标识，已存在于连接池，则用 <see cref="signalRGuid"/> 更新旧的 Guid 。</para>
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="userPoolGuid">用户池token标识</param>
        /// <param name="groupName">分组名</param>
        /// <param name="signalRGuid">SignalRID</param>
        /// <returns></returns>
        public static bool AddConnection(string userName, Guid userPoolGuid, string groupName, Guid signalRGuid)
        {
            return MemberToken.AddConnection(userName, userPoolGuid, groupName, signalRGuid);
        }

        /// <summary>
        /// 连接服务的核心 API 之一；对指定的用户名和指定的分组名执行移除连接操作
        /// <para>该用户名在该分组中的所有设备上的连接都会被移除。</para>
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="groupName">分组名</param>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<Guid, bool>> RemoveConnection(string userName, string groupName)
        {
            return MemberToken.RemoveConnection(userName, groupName);
        }

        /// <summary>
        /// 连接服务的核心 API 之一；对指定的连接身份的标识进行移除连接操作。
        /// <para>该连接身份的标识对应的用户的连接会被移除。</para>
        /// </summary>
        /// <param name="tokenGuid">连接身份的标识</param>
        /// <returns></returns>
        public static bool RemoveConnection(Guid tokenGuid)
        {
            return MemberToken.RemoveConnection(tokenGuid);
        }

        #endregion

        #region 获取连接状态相关信息

        /// <summary>
        /// 获取在指定的 <see cref="groupName"/> 中指定的 <see cref="userName"/> 的所有连接信息的标识ID集合。
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="groupName">分组名</param>
        /// <returns></returns>
        public static IEnumerable<string> GetTokenGuid(string userName, string groupName)
        {
            return MemberToken.GetTokenByUserName(userName, groupName).Select(s => s.Guid.ToString());
        }

        /// <summary>
        /// 获取指定的 <see cref="key"/> 对应的连接用户信息。
        /// </summary>
        /// <param name="key">连接身份标识</param>
        /// <returns></returns>
        public static MemberToken GetToken(Guid key)
        {
            return MemberToken.GetTokenByGuid(key);
        }

        #endregion

        #region 公共方法定义 - 清空用户池

        /// <summary>
        /// 清空连接池
        /// </summary>
        public static void ClearMemberPool()
        {
            MemberToken.Tokens.Clear();
        }

        #endregion
    }
}
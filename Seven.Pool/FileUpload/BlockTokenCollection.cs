﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Seven.Tools;
using Seven.Tools.Extension;
using Seven.Tools.Collections.Concurrent;

namespace Seven.Pool.FileUpload
{
    public class BlockTokenCollection : ObservableConcurrentDictionary<string, BlockToken>
    {
        private object _locker = new object();
    }
}
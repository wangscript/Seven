﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Seven.Entity;

namespace Seven.MsSql.Repositories
{
    public partial class HrOrganizationRepository
    {
        /// <summary>
        /// 根据用户ID获取该用户的组织结构管理范围集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        public IEnumerable<HrOrganization> GetRangeOrgans(int userId)
        {
            return this.Entities().ToArray();
        }

        /// <summary>
        /// 根据父级ID获取下一级组织结构集合
        /// </summary>
        /// <param name="parentId">父级ID</param>
        /// <param name="orderField">排序字段名称，默认为 SortNumber </param>
        /// <param name="sortDirection">排序方式，默认顺序</param>
        /// <returns></returns>
        public IEnumerable<HrOrganization> GetSons(int parentId, string orderField = "SortNumber", ListSortDirection sortDirection = ListSortDirection.Ascending)
        {
            if (!string.IsNullOrWhiteSpace(orderField))
            {
                if (orderField.Equals("SortNumber", StringComparison.InvariantCultureIgnoreCase))
                {
                    var query = this.Entities().Where(w => w.ParentID == parentId && !w.IsDeleted);
                    if (sortDirection == ListSortDirection.Ascending)
                    { return query.OrderBy(o => o.SortNumber).ToArray(); }
                    else
                    { return query.OrderByDescending(o => o.SortNumber).ToArray(); }
                }
                else
                { return this.Get(w => w.ParentID == parentId && !w.IsDeleted, orderField, sortDirection); }
            }
            else
            { return this.Get(w => w.ParentID == parentId && !w.IsDeleted); }
        }

        /// <summary>
        /// 根据组织结构ID集合，获取他们的下一级组织结构的数量，以Dic形式返回数据，其中Key为ids的元素，Value为其下一级组织结构数量
        /// </summary>
        /// <param name="ids">组织结构ID集合</param>
        /// <returns></returns>
        public IDictionary<int, int> GetSonsCount(IEnumerable<int> ids)
        {
            var result = new Dictionary<int, int>();
            if (ids.Count() == 0) { return result; }

            var data = this.Entities().Where(w => ids.Contains(w.ParentID)).GroupBy(g => g.ParentID).Select(s => new { s.Key, Value = s.Count() });
            foreach (var item in data)
            {
                result.Add(item.Key, item.Value);
            }

            return result;
        }

        /// <summary>
        /// 根据组织结构code集合，获取他们的所属公司ID，以Dic形式返回数据，其中Key为codes的元素，Value为其所属公司的ID
        /// </summary>
        /// <param name="codes">组织结构code集合</param>
        /// <returns></returns>
        public IDictionary<string, int> GetCompanyIDs(IEnumerable<string> codes)
        {
            var result = new Dictionary<string, int>();
            if (codes.Count() == 0) { return result; }

            var codesTemp = codes.Where(w => w.Length > 1).Select(s => s.Substring(0, 2)).Distinct();
            var data = this.Entities().Where(w => codesTemp.Contains(w.Code)).Select(s => new { s.ID, s.Code }).ToArray();

            foreach (var item in codes)
            {
                var f = data.Where(w => w.Code == item.Substring(0, 2)).Select(s => s.ID).FirstOrDefault();
                result.Add(item, f == 0 ? -1 : f);
            }

            return result;
        }

        /// <summary>
        /// 获取公司级别的组织结构集合
        /// </summary>
        /// <returns></returns>
        public IEnumerable<HrOrganization> GetCompanys()
        {
            return this.Entities().Where(w => w.Type == EntityBasic.OrganType.公司 && !w.IsDeleted).OrderBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 根据父级code获取所有子级组织结构集合
        /// </summary>
        /// <param name="code">父级code</param>
        /// <param name="showSelf">是否显示父级code对应的组织结构</param>
        /// <returns></returns>
        public IEnumerable<HrOrganization> GetChildren(string code, bool showSelf = false)
        {
            var query = this.Entities().Where(w => w.Code.StartsWith(code) && !w.IsDeleted);
            if (!showSelf) { query = query.Where(w => w.Code != code); }

            return query.OrderBy(o => o.SortNumber).ToArray();
        }
    }
}

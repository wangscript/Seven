﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.EntityBasic;
using Seven.DataModel.RepositoryModels.DeskModuleAbout;
using Seven.DataModel.ViewModels.DeskModuleAbout;
using Seven.Tools.Extension;

namespace Seven.MsSql.Repositories
{
    public partial class SysDeskModuleRepository
    {
        /// <summary>
        /// 根据桌面模块ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">桌面模块ID</param>
        /// <returns></returns>
        public DeskModuleModel GetEditModelByID(int id)
        {
            var menu = new SysMenuRepository(this.dbContext).Entities();
            var data = (from a in this.Entities().Where(w => w.ID == id)
                        join b in menu on a.MenuID equals b.ID into ab
                        from menuInfo in ab.DefaultIfEmpty()
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.Location,
                            a.ControlType,
                            a.SortNumber,
                            a.Size,
                            a.Height,
                            a.ReceiveMessage,
                            a.ActionName,
                            a.IsDisabled,
                            a.IsLimited,
                            a.MenuID,
                            MenuName = menuInfo == null ? "" : menuInfo.Name
                        }).FirstOrDefault();

            return data.CastTo<DeskModuleModel>();
        }

        /// <summary>
        /// 获取指定位置的桌面模块列表数据
        /// </summary>
        /// <param name="location">位置</param>
        /// <returns></returns>
        public IEnumerable<DeskModuleGridModel> DeskModuleGrid(DeskModuleLocation? location)
        {
            var deskModule = this.Entities();
            if (location.HasValue)
            {
                deskModule = deskModule.Where(w => w.Location == location);
            }

            var menu = new SysMenuRepository(this.dbContext).Entities();

            var query = (from a in deskModule
                         join b in menu on a.MenuID equals b.ID into ab
                         from menuInfo in ab.DefaultIfEmpty()
                         select new
                         {
                             a.ID,
                             a.Name,
                             a.Size,
                             a.Height,
                             a.Location,
                             a.ReceiveMessage,
                             a.ControlType,
                             a.SortNumber,
                             a.ActionName,
                             a.IsDisabled,
                             a.IsLimited,
                             MenuName = menuInfo == null ? "" : menuInfo.Name
                         });
            var result = query.OrderBy(o => o.SortNumber).ToArray();

            return result.Select(s => s.CastTo<DeskModuleGridModel>(false));
        }

        /// <summary>
        /// 获取所有可用的桌面模块列表数据
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SysDeskModule> GetUseableDeskModules()
        {
            return this.Get(w => !w.IsDisabled).OrderBy(o => o.SortNumber);
        }

        /// <summary>
        /// 根据主键ID集合获取桌面模块集合
        /// </summary>
        /// <param name="ids">主键ID集合</param>
        /// <returns></returns>
        public IEnumerable<SysDeskModule> GetDeskModules(IEnumerable<int> ids)
        {
            return this.Get(g => ids.Contains(g.ID));
        }
    }
}

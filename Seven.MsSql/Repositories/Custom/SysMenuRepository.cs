﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.DataModel.RepositoryModels.MenuAbout;
using Seven.DataModel.ViewModels.MenuAbout;
using Seven.Tools.Extension;

namespace Seven.MsSql.Repositories
{
    public partial class SysMenuRepository
    {
        #region 获取简单模型数据

        public SimpleModel1 GetSimpleModelByID(int id)
        {
            var data = this.Entities().Where(w => w.ID == id).Select(s => new { s.Name, s.ControllerName, s.ActionName, FunctionCount = s.MenuFunctions.Count }).FirstOrDefault();

            return data.CastTo<SimpleModel1>();
        }

        #endregion

        /// <summary>
        /// 根据菜单ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns></returns>
        public MenuModel GetEditModelByID(int id)
        {
            var data = this.Entities().Where(w => w.ID == id)
                .Select(s => new
                {
                    s.ID,
                    s.Name,
                    s.Code,
                    s.SortNumber,
                    s.IconCls,
                    s.ParentID,
                    s.ControllerName,
                    s.ActionName,
                    s.IsShow,
                    s.IsSystem,
                    s.Remark
                })
                .FirstOrDefault();

            return data.CastTo<MenuModel>();
        }

        /// <summary>
        /// 获取所有菜单列表
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SysMenu> GetAllMenus()
        {
            return this.Entities().OrderBy(o => o.ParentID).ThenBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 根据菜单ID获取菜单名称
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns></returns>
        public string GetMenuName(int id)
        {
            var data = this.Entities().Where(w => w.ID == id).Select(s => s.Name).FirstOrDefault();
            return data;
        }

        /// <summary>
        /// 根据菜单ID获取菜单code
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns></returns>
        public string GetMenuCode(int id)
        {
            var data = this.Entities().Where(w => w.ID == id).Select(s => s.Code).FirstOrDefault();
            return data;
        }

        /// <summary>
        /// 根据菜单ID判定该菜单下是否存在子菜单
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns></returns>
        public bool SonMenuExist(int menuId)
        {
            return this.Any(w => w.ParentID == menuId);
        }

        /// <summary>
        /// 根据菜单ID获取同级菜单中最大的排序号
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns></returns>
        public int GetMaxSortNumberNearLevel(int menuId)
        {
            return this.Max(w => w.ParentID == menuId, m => m.SortNumber);
        }

        /// <summary>
        /// 根据菜单code判定该code是否已经存在
        /// </summary>
        /// <param name="menuCode">菜单code</param>
        /// <returns></returns>
        public bool MenuCodeExist(string menuCode)
        {
            return this.Any(w => w.Code == menuCode);
        }

        /// <summary>
        /// 根据父级ID获取下一级菜单集合
        /// </summary>
        /// <param name="parentId">父级ID</param>
        /// <returns></returns>
        public IEnumerable<SysMenu> GetSons(int parentId)
        {
            return this.Entities().Where(w => w.ParentID == parentId).OrderBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 根据菜单ID集合，获取他们的下一级菜单的数量，以Dic形式返回数据，其中Key为ids的元素，Value为其下一级菜单数量
        /// </summary>
        /// <param name="ids">菜单ID集合</param>
        /// <returns></returns>
        public IDictionary<int, int> GetSonsCount(IEnumerable<int> ids)
        {
            var result = new Dictionary<int, int>();
            if (ids.Count() == 0) { return result; }

            var data = this.Entities().Where(w => ids.Contains(w.ParentID)).GroupBy(g => g.ParentID).Select(s => new { s.Key, Value = s.Count() });
            foreach (var item in data)
            {
                result.Add(item.Key, item.Value);
            }

            return result;
        }

        /// <summary>
        /// 根据菜单code获取其所有下级菜单
        /// </summary>
        /// <param name="menuCode">菜单code</param>
        /// <returns></returns>
        public IEnumerable<SysMenu> GetChildren(string menuCode)
        {
            return this.Entities().Where(g => g.Code.StartsWith(menuCode) && g.Code != menuCode).OrderBy(o => o.SortNumber).ToArray();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Seven.MsSql
{
    /// <summary>
    /// 上下文主要配置
    /// </summary>
    public static class DbConfig
    {
        /// <summary>
        /// 连接字符串
        /// </summary>
        public static string ConnString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;

        /// <summary>
        /// MsSql ProviderName
        /// </summary>
        public const string ProviderName = "System.Data.SqlClient";
    }
}

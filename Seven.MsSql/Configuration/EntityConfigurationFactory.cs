﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.MsSql.Configuration
{
    /// <summary>
    /// 实体配置工厂类
    /// </summary>
    public static class EntityConfigurationFactory
    {
        /// <summary>
        /// 同步对象
        /// </summary>
        private static readonly object sync = new object();

        /// <summary>
        /// 唯一实例
        /// </summary>
        private static IEnumerable<IEntityConfiguration> singleton;

        /// <summary>
        /// 实体配置
        /// </summary>
        public static IEnumerable<IEntityConfiguration> Configurations
        {
            get
            {
                if (singleton == null)
                {
                    lock (sync)
                    {
                        if (singleton == null)
                        {
                            var types = AppDomain.CurrentDomain.GetAssemblies().Where(w => w.FullName.Contains("Seven.MsSql"))
                                .SelectMany(m => m.GetTypes())
                                .Where(m => m.GetInterfaces().Contains(typeof(IEntityConfiguration)) && !m.IsAbstract);

                            singleton = types.Select(m => Activator.CreateInstance(m) as IEntityConfiguration);
                        }
                    }
                }

                return singleton;
            }
        }
    }
}

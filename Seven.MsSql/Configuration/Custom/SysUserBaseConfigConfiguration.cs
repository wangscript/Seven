﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.MsSql.Configuration
{
    partial class SysUserBaseConfigConfiguration
    {
        partial void SysUserBaseConfigConfigurationAppend()
        {
            HasRequired(m => m.User).WithMany().HasForeignKey(f => f.UserID);
        }
    }
}

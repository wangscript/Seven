﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.MsSql.Configuration
{
    partial class HrStaffConfiguration
    {
        partial void HrStaffConfigurationAppend()
        {
            HasRequired(r => r.Dept).WithMany().HasForeignKey(f => f.DeptID);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.EntityBasic;

namespace Seven.DataModel.ViewModels.MenuAbout
{
    public class MenuPermissionModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int ID { get; set; }

        // <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 按钮名称
        /// </summary>
        public string ButtonName { get; set; }

        /// <summary>
        /// 图标样式
        /// </summary>
        public string IconCls { get; set; }

        /// <summary>
        /// 按钮点击事件名称
        /// </summary>
        public string HandlerName { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public PermissionType Type { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SortNumber { get; set; }

        /// <summary>
        /// 分组标记
        /// </summary>
        public string GroupMark { get; set; }

        /// <summary>
        /// 所属菜单ID
        /// </summary>
        public int MenuID { get; set; }

        /// <summary>
        /// 所属菜单名称
        /// </summary>
        public string MenuName { get; set; }

        /// <summary>
        /// 菜单功能点ID串
        /// </summary>
        public string FunctionIDs { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public bool IsShow { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}

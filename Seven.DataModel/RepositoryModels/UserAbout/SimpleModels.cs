﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.EntityBasic;

namespace Seven.DataModel.RepositoryModels.UserAbout
{
    public class SimpleModel1
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 员工姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 员工性别
        /// </summary>
        public Sex Sex { get; set; }
    }
}

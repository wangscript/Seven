﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Seven.EntityBasic
{
    public abstract class EntityBase<TKey> : IEntityBase where TKey : struct
    {
        #region 属性

        [Key]
        public TKey ID { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        [Display(Name = "创建人")]
        [Required]
        public string CreateUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 最后修改人
        /// </summary>
        [Display(Name = "最后修改人")]
        public string LastModifyUserName { get; set; }

        /// <summary>
        /// 最后修改时间
        /// </summary>
        [Display(Name = "最后修改时间")]
        public DateTime? LastModifyDate { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Display(Name = "备注")]
        public string Remark { get; set; }

        /// <summary>
        /// 上次改动时间戳，数据库内置时间。用来实现乐观并发效果。
        /// </summary>
        [Timestamp]
        [ConcurrencyCheck]
        public byte[] Timestamp { get; set; }

        #endregion
    }

    /// <summary>
    /// 用来便捷判定实体类的接口，不用判定是否继承于EntityBase类，因为该类需要T。目前是T4用到。
    /// </summary>
    public interface IEntityBase
    { }
}

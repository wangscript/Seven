﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("HrOrganization")]
    public class HrOrganization : EntityBase<int>
    {
        /// <summary>
        /// 机构名称
        /// </summary>
        [Display(Name = "机构名称")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 机构名称前缀
        /// </summary>
        [Display(Name = "机构名称前缀")]
        public string PrefixName { get; set; }

        /// <summary>
        /// 机构名称后缀
        /// </summary>
        [Display(Name = "机构名称后缀")]
        public string PostfixName { get; set; }

        /// <summary>
        /// 机构简称
        /// </summary>
        [Display(Name = "机构简称")]
        public string ShortName { get; set; }

        /// <summary>
        /// 机构编号
        /// </summary>
        [Display(Name = "机构编号")]
        public string Code { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        [Display(Name = "排序号")]
        public int SortNumber { get; set; }

        /// <summary>
        /// 机构类型
        /// </summary>
        [Display(Name = "机构类型")]
        public OrganType Type { get; set; }

        /// <summary>
        /// 所属机构
        /// </summary>
        [Display(Name = "父级机构ID")]
        public int ParentID { get; set; }

        /// <summary>
        /// 是否被删除
        /// </summary>
        [Display(Name = "是否被删除")]
        public bool IsDeleted { get; set; }



        /// <summary>
        /// 机构的全名
        /// 公司，显示短名
        /// 部门，显示“前缀-Name(后缀)”。若无前缀，则显示“Name(后缀)”；若无后缀则显示“前缀-Name”。
        /// </summary>
        public string FullName
        {
            get
            {
                if (Type == OrganType.部门) {
                    bool prefixSpace = string.IsNullOrWhiteSpace(PrefixName);
                    bool postfixSpace = string.IsNullOrWhiteSpace(PostfixName);
                    string result = string.Empty;
                    if (!prefixSpace) { result = (PrefixName + "-") + Name; }
                    else { result = Name; }
                    if (!postfixSpace) { result = result + ("(" + PostfixName + ")"); }

                    return result;
                }
                else { return Type == OrganType.公司 ? ShortName : Name; }
            }
        }
    }
}

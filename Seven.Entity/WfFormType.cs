﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("WfFormType")]
    public class WfFormType : EntityBase<int>
    {
        /// <summary>
        /// 类型名称
        /// </summary>
        [Display(Name = "类型名称")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        [Display(Name = "排序号")]
        public int SortNumber { get; set; }

        /// <summary>
        /// 父级类别
        /// </summary>
        [Display(Name = "父级类别")]
        public int ParentID { get; set; }

        /// <summary>
        /// 所属公司
        /// </summary>
        [Display(Name = "所属公司")]
        public int CompanyID { get; set; }

        /// <summary>
        /// 是否禁用
        /// </summary>
        [Display(Name = "是否禁用")]
        public bool IsDisabled { get; set; }
    }
}

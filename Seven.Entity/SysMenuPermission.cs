﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("SysMenuPermission")]
    public class SysMenuPermission : EntityBase<int>
    {
        /// <summary>
        /// 权限名称
        /// </summary>
        [Display(Name = "权限名称")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 按钮名称
        /// </summary>
        [Display(Name = "按钮名称")]
        public string ButtonName { get; set; }

        /// <summary>
        /// 图标样式
        /// </summary>
        [Display(Name = "图标样式")]
        public string IconCls { get; set; }

        /// <summary>
        /// 按钮点击事件名称
        /// </summary>
        [Display(Name = "按钮点击事件名称")]
        public string HandlerName { get; set; }

        /// <summary>
        /// 分组标记
        /// </summary>
        [Display(Name = "分组标记")]
        public string GroupMark { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [Display(Name = "类型")]
        public PermissionType Type { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        [Display(Name = "排序号")]
        public int SortNumber { get; set; }

        /// <summary>
        /// 菜单ID
        /// </summary>
        [Display(Name = "菜单ID")]
        public int MenuID { get; set; }

        /// <summary>
        /// 菜单
        /// </summary>
        [Display(Name = "菜单")]
        public virtual SysMenu Menu { get; set; }

        /// <summary>
        /// 菜单功能点ID串
        /// </summary>
        [Display(Name = "菜单功能点ID串")]
        public string FunctionIDs { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        [Display(Name = "是否显示")]
        public bool IsShow { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("SysRole")]
    public class SysRole : EntityBase<int>
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        [Display(Name = "角色名称")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        [Display(Name = "编号")]
        [Required]
        public string Code { get; set; }

        /// <summary>
        /// 所属公司ID
        /// </summary>
        [Display(Name = "所属公司ID")]
        public int CompanyID { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        [Display(Name = "排序号")]
        public int SortNumber { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        [Display(Name = "图标")]
        public string Icon { get; set; }

        /// <summary>
        /// 是否超级管理员
        /// </summary>
        [Display(Name = "是否超级管理员")]
        public bool IsSuperAdmin { get; set; }

        /// <summary>
        /// 是否禁用
        /// </summary>
        [Display(Name = "是否禁用")]
        public bool IsDisabled { get; set; }

        /// <summary>
        /// 禁用人
        /// </summary>
        [Display(Name = "禁用人")]
        public string DisableUserName { get; set; }

        /// <summary>
        /// 禁用日期
        /// </summary>
        [Display(Name = "禁用日期")]
        public DateTime? DisableDate { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        [Display(Name = "是否删除")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// 删除人
        /// </summary>
        [Display(Name = "删除人")]
        public string Deleteder { get; set; }

        /// <summary>
        /// 删除时间
        /// </summary>
        [Display(Name = "删除时间")]
        public DateTime? DeletedDate { get; set; }
    }
}

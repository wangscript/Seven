﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("WfForm")]
    public class WfForm : EntityBase<int>
    {
        /// <summary>
        /// 表单名称
        /// </summary>
        [Display(Name = "表单名称")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 所属类型
        /// </summary>
        [Display(Name = "所属类型")]
        public int Type { get; set; }

        /// <summary>
        /// 表单内容
        /// </summary>
        [Display(Name = "表单内容")]
        public string Content { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        [Display(Name = "排序号")]
        public int SortNumber { get; set; }
    }
}

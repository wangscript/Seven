﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("WfFormItem")]
    public class WfFormItem : EntityBase<int>
    {
        /// <summary>
        /// 所属表单
        /// </summary>
        [Display(Name = "所属表单")]
        public int FormID { get; set; }

        /// <summary>
        /// 表单项目序号
        /// </summary>
        [Display(Name = "表单项目序号")]
        public int ItemNumber { get; set; }

        /// <summary>
        /// 表单项目名称
        /// </summary>
        [Display(Name = "表单项目名称")]
        public string ItemName { get; set; }

        /// <summary>
        /// 表单项目类型
        /// </summary>
        [Display(Name = "表单项目类型")]
        public string ItemType { get; set; }

        /// <summary>
        /// 表单项目网页标签名
        /// </summary>
        [Display(Name = "表单项目网页标签名")]
        public string ItemTagName { get; set; }

        /// <summary>
        /// 表单项目字段长度
        /// </summary>
        [Display(Name = "表单项目字段长度")]
        public int Length { get; set; }

        /// <summary>
        /// 是否隐藏
        /// </summary>
        [Display(Name = "是否隐藏")]
        public bool Hidden { get; set; }

        /// <summary>
        /// 组名
        /// </summary>
        [Display(Name = "组名")]
        public string GroupName { get; set; }
    }
}

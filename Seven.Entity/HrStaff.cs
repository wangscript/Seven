﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("HrStaff")]
    public class HrStaff : EntityBase<int>
    {
        /// <summary>
        /// 姓名
        /// </summary>
        [Display(Name = "姓名")]
        public string RealName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [Display(Name = "性别")]
        public Sex Sex { get; set; }

        /// <summary>
        /// 所在集团ID
        /// </summary>
        [Display(Name = "所在集团ID")]
        public int GroupID { get; set; }

        /// <summary>
        /// 所在公司ID
        /// </summary>
        [Display(Name = "所在公司ID")]
        public int CompanyID { get; set; }

        /// <summary>
        /// 所在部门ID
        /// </summary>
        [Display(Name = "所在部门ID")]
        public int DeptID { get; set; }

        public virtual HrOrganization Dept { get; set; }

        /// <summary>
        /// 身高
        /// </summary>
        [Display(Name = "身高")]
        public int height { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>
        [Display(Name = "出生日期")]
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        [Display(Name = "是否删除")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// 删除人
        /// </summary>
        [Display(Name = "删除人")]
        public string Deleteder { get; set; }

        /// <summary>
        /// 删除时间
        /// </summary>
        [Display(Name = "删除时间")]
        public DateTime? DeletedDate { get; set; }
    }
}

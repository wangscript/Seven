﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("SysUserMenuPermission")]
    public class SysUserMenuPermission : EntityBase<int>
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        [Display(Name = "用户ID")]
        public int UserID { get; set; }

        /// <summary>
        /// 菜单权限ID
        /// </summary>
        [Display(Name = "菜单权限ID")]
        public int MenuPermissionID { get; set; }

        /// <summary>
        /// 选中且打勾，忽略角色权限
        /// 选中不打勾，根据角色权限
        /// 不选中，忽略角色权限
        /// </summary>
        [Display(Name = "选中状态")]
        public bool Check { get; set; }

        /// <summary>
        /// 选中且打勾，忽略角色权限
        /// 选中不打勾，根据角色权限
        /// 不选中，忽略角色权限
        /// </summary>
        [Display(Name = "打勾状态")]
        public bool Indeterminate { get; set; }
    }
}

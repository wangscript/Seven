﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Core.Helper
{
    /// <summary>
    /// 单例工具
    /// </summary>
    /// <typeparam name="T">类型</typeparam>
    public static class SingletonHelper<T> where T : class, new()
    {
        /// <summary>
        /// 同步对象
        /// </summary>
        private static readonly object sync = new object();

        /// <summary>
        /// 唯一实例
        /// </summary>
        private static T singleton;

        /// <summary>
        /// 实例对象
        /// </summary>
        public static T Instance
        {
            get
            {
                if (singleton == null)
                {
                    lock (sync)
                    {
                        if (singleton == null)
                        {
                            singleton = new T();
                        }
                    }
                }

                return singleton;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.DataModel.RepositoryModels.RoleAbout;

namespace Seven.Core.IRepositories
{
    public partial interface ISysRoleMenuPermissionRepository
    {
        /// <summary>
        /// 获取指定角色的限定菜单权限列表数据
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <param name="menuIds">菜单ID集合</param>
        /// <param name="filterUncheck">是否过滤掉“未选中”数据</param>
        /// <returns></returns>
        IEnumerable<RoleMenuPermissionGridModel> RoleMenuPermissionGrid(int roleId, IEnumerable<int> menuIds, bool filterUncheck);

        /// <summary>
        /// 根据角色ID获取该角色的菜单权限集合
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <returns></returns>
        IEnumerable<SysRoleMenuPermission> GetRoleMenuPermissions(int roleId);

        /// <summary>
        /// 根据用户ID获取该用户的角色的菜单权限集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        IEnumerable<SysRoleMenuPermission> GetRoleMenuPermissionsByUserID(int userId);

        /// <summary>
        /// 根据用户ID获取该用户对应的角色的有权使用的菜单权限集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        IEnumerable<SysMenuPermission> GetRangeMenuPermissions(int userId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;

namespace Seven.Core.IRepositories
{
    public partial interface ISysUserBaseConfigRepository
    {
        /// <summary>
        /// 根据用户ID获取该用户的基本配置信息【无配置信息则返回新实例】
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        SysUserBaseConfig GetUserBaseConfig(int userId);
    }
}

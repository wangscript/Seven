﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.DataModel.ViewModels.MenuAbout;

namespace Seven.Core.IRepositories
{
    public partial interface ISysMenuPermissionRepository
    {
        /// <summary>
        /// 根据菜单权限ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">菜单权限ID</param>
        /// <returns></returns>
        MenuPermissionModel GetEditModelByID(int id);

        /// <summary>
        /// 根据菜单ID获取菜单权限集合
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns></returns>
        IEnumerable<SysMenuPermission> GetMenuPermissionsByMenuID(int menuId);

        /// <summary>
        /// 根据菜单ID集合获取菜单权限集合
        /// </summary>
        /// <param name="menuIds">菜单ID集合</param>
        /// <returns></returns>
        IEnumerable<SysMenuPermission> GetMenuPermissionsByMenuIDs(IEnumerable<int> menuIds);

        /// <summary>
        /// 获取所有菜单权限集合
        /// </summary>
        /// <returns></returns>
        IEnumerable<SysMenuPermission> GetAllMenuPermissions();

        /// <summary>
        /// 获取除指定id之外的类型为按钮的菜单权限集合
        /// </summary>
        /// <param name="ids">要排除的菜单权限ID集合</param>
        /// <returns></returns>
        IEnumerable<SysMenuPermission> GetMenuButtonPermissionsExcept(IEnumerable<int> ids);

        /// <summary>
        /// 根据菜单权限ID集合获取菜单权限集合
        /// </summary>
        /// <param name="ids">菜单权限ID集合</param>
        /// <returns></returns>
        IEnumerable<SysMenuPermission> GetMenuPermissions(IEnumerable<int> ids);

        /// <summary>
        /// 获取除指定菜单权限ID集合之外的菜单权限集合
        /// </summary>
        /// <param name="ids">要排除的菜单权限ID集合</param>
        /// <returns></returns>
        IEnumerable<SysMenuPermission> GetMenuPermissionsExcept(IEnumerable<int> ids);

        /// <summary>
        /// 根据菜单权限ID集合获取菜单权限记录中的functionids集合
        /// </summary>
        /// <param name="ids">菜单权限ID集合</param>
        /// <returns></returns>
        IEnumerable<string> GetMenuFunctionIDs(IEnumerable<int> ids);
    }
}

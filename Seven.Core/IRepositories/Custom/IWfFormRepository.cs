﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.DataModel.RepositoryModels.WorkflowAbout;
using Seven.DataModel.ViewModels.WorkflowAbout;

namespace Seven.Core.IRepositories
{
    public partial interface IWfFormRepository
    {
        /// <summary>
        /// 根据表单ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">表单ID</param>
        /// <returns></returns>
        FormModel GetEditModelByID(int id);

        /// <summary>
        /// 根据表单ID获取供设计的数据模型
        /// </summary>
        /// <param name="id">表单ID</param>
        /// <returns></returns>
        FormDesignModel GetDesignModelByID(int id);

        /// <summary>
        /// 根据表单类型ID获取表单数据模型集合
        /// </summary>
        /// <param name="typeId">表单类型ID</param>
        /// <param name="model">序列化的查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <param name="total">数据总数，输出参数</param>
        /// <returns></returns>
        IEnumerable<FormGridModel> FormGrid(int typeId, string model, int page, int rows, out int total);
    }
}

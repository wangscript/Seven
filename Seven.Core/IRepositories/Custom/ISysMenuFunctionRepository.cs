﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.DataModel.RepositoryModels.MenuAbout;
using Seven.DataModel.ViewModels.MenuAbout;

namespace Seven.Core.IRepositories
{
    public partial interface ISysMenuFunctionRepository
    {
        /// <summary>
        /// 根据菜单功能点ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">菜单功能点ID</param>
        /// <returns></returns>
        MenuFunctionModel GetEditModelByID(int id);

        /// <summary>
        /// 根据菜单ID获取菜单功能点集合
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns></returns>
        IEnumerable<SysMenuFunction> GetMenuFunctionsByMenuID(int menuId);

        /// <summary>
        /// 根据菜单功能点ID集合获取菜单功能点集合
        /// </summary>
        /// <param name="ids">菜单功能点ID集合</param>
        /// <param name="includeMenu">是否同时查询导航属性-菜单的数据</param>
        /// <returns></returns>
        IEnumerable<SysMenuFunction> GetMenuFunctionsByIDs(IEnumerable<int> ids, bool includeMenu = false);

        /// <summary>
        /// 获取所有菜单功能点集合
        /// </summary>
        /// <param name="includeMenu">是否同时查询导航属性-菜单的数据</param>
        /// <returns></returns>
        IEnumerable<SysMenuFunction> GetAllMenuFunctions(bool includeMenu = false);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Seven.Entity;

namespace Seven.Core.IRepositories
{
    /// <summary>
    /// 仓储接口
    /// </summary>
    public partial interface IHrOrganizationRepository
    {
        /// <summary>
        /// 根据用户ID获取该用户的组织结构管理范围集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        IEnumerable<HrOrganization> GetRangeOrgans(int userId);

        /// <summary>
        /// 根据父级ID获取下一级组织结构集合
        /// </summary>
        /// <param name="parentId">父级ID</param>
        /// <param name="orderField">排序字段名称，默认为 SortNumber </param>
        /// <param name="sortDirection">排序方式，默认顺序</param>
        /// <returns></returns>
        IEnumerable<HrOrganization> GetSons(int parentId, string orderField = "SortNumber", ListSortDirection sortDirection = ListSortDirection.Ascending);

        /// <summary>
        /// 根据组织结构ID集合，获取他们的下一级组织结构的数量，以Dic形式返回数据，其中Key为ids的元素，Value为其下一级组织结构数量
        /// </summary>
        /// <param name="ids">组织结构ID集合</param>
        /// <returns></returns>
        IDictionary<int, int> GetSonsCount(IEnumerable<int> ids);

        /// <summary>
        /// 根据组织结构code集合，获取他们的所属公司ID，以Dic形式返回数据，其中Key为codes的元素，Value为其所属公司的ID
        /// </summary>
        /// <param name="codes">组织结构code集合</param>
        /// <returns></returns>
        IDictionary<string, int> GetCompanyIDs(IEnumerable<string> codes);

        /// <summary>
        /// 获取公司级别的组织结构集合
        /// </summary>
        /// <returns></returns>
        IEnumerable<HrOrganization> GetCompanys();

        /// <summary>
        /// 根据父级code获取所有子级组织结构集合
        /// </summary>
        /// <param name="code">父级code</param>
        /// <param name="showSelf">是否显示父级code对应的组织结构</param>
        /// <returns></returns>
        IEnumerable<HrOrganization> GetChildren(string code, bool showSelf = false);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Core.IInitialize
{
    public interface IDatabaseInitializer
    {
        void Initialize(bool migrate = true);
    }
}

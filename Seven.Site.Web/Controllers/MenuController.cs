﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using Seven.DataModel.ViewModels.MenuAbout;
using Seven.Service;
using Seven.Site.Mvc.Attributes;
using Seven.Site.Mvc.Filters;

namespace Seven.Site.Web.Controllers
{
    public class MenuController : BaseController
    {
        #region 页面

        #region 菜单页面

        /// <summary>
        /// 菜单管理页面
        /// </summary>
        /// <returns></returns>
        [Description("菜单管理页面")]
        [GetButtonPermissions("menu,menuFunction")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public ViewResult ManageIndex()
        {
            return View();
        }

        /// <summary>
        /// 新建菜单页面
        /// </summary>
        /// <param name="parentId">父级菜单ID</param>
        /// <returns></returns>
        [Description("新建菜单页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult MenuCreateForm(int parentId = 0)
        {
            var model = ServiceFactory.SysMenu.GetNewMenuModel(parentId);

            return PartialView(model);
        }

        /// <summary>
        /// 编辑菜单页面
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns></returns>
        [Description("编辑菜单页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult MenuEditForm(int id)
        {
            var model = ServiceFactory.SysMenu.GetEditMenuModel(id);

            return PartialView("MenuCreateForm", model);
        }

        #endregion

        #region 菜单功能点页面

        /// <summary>
        /// 新建菜单功能点页面
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns></returns>
        [Description("新建菜单功能点页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult MenuFunctionCreateForm(int menuId)
        {
            var model = ServiceFactory.SysMenuFunction.GetNewMenuFunctionModel(menuId);

            return PartialView(model);
        }

        /// <summary>
        /// 编辑菜单功能点页面
        /// </summary>
        /// <param name="id">菜单功能点ID</param>
        /// <returns></returns>
        [Description("编辑菜单权限点页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult MenuFunctionEditForm(int id)
        {
            var model = ServiceFactory.SysMenuFunction.GetEditMenuFunctionModel(id);

            return PartialView("MenuFunctionCreateForm", model);
        }

        #endregion

        #region 菜单权限配置页面

        /// <summary>
        /// 菜单权限配置页面
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <param name="menuName">菜单名称</param>
        /// <returns></returns>
        [Description("菜单权限配置页面")]
        [GetButtonPermissions("menuPermission", "ManageIndex")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult MenuPermissionManageIndex(int menuId, string menuName)
        {
            ViewBag.MenuID = menuId;
            ViewBag.MenuName = Server.UrlDecode(menuName);

            return PartialView();
        }

        /// <summary>
        /// 新建菜单权限页面
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <param name="menuName">菜单名称</param>
        /// <returns></returns>
        [Description("新建菜单权限页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult MenuPermissionCreateForm(int menuId, string menuName)
        {
            var model = ServiceFactory.SysMenuPermission.GetNewMenuPermissionModel(menuId, Server.UrlDecode(menuName));

            return PartialView(model);
        }

        /// <summary>
        /// 编辑菜单权限页面
        /// </summary>
        /// <param name="id">菜单权限ID</param>
        /// <param name="menuName">菜单名称</param>
        /// <returns></returns>
        [Description("编辑菜单权限页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult MenuPermissionEditForm(int id, string menuName)
        {
            var model = ServiceFactory.SysMenuPermission.GetEditMenuPermissionModel(id);
            model.MenuName = Server.UrlDecode(menuName);

            return PartialView("MenuPermissionCreateForm", model);
        }

        #endregion

        #endregion

        #region 数据

        /// <summary>
        /// 以异步方式为treegrid提供菜单信息json数据
        /// </summary>
        /// <param name="id">父级菜单ID</param>
        /// <returns></returns>
        [Description("以异步方式为treegrid提供菜单信息json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult MenuJsonForTreeGrid(int id = 0)
        {
            return ServiceFactory.SysMenu.MenuJsonForTreeGrid(id);
        }

        /// <summary>
        /// 提供功能点类型json数据
        /// </summary>
        /// <returns></returns>
        [Description("提供功能点类型json数据")]
        [Login]
        [CatchException(ActionType.数据)]
        public JsonResult FunctionTypeJson()
        {
            return ServiceFactory.SysMenuFunction.FunctionTypeJson();
        }

        /// <summary>
        /// 提供菜单功能点json数据
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns></returns>
        [Description("提供菜单功能点json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult MenuFunctionJson(int menuId)
        {
            return ServiceFactory.SysMenuFunction.MenuFunctionJson(menuId);
        }

        /// <summary>
        /// 提供菜单权限json数据
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns></returns>
        [Description("提供菜单权限json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult MenuPermissionJson(int menuId)
        {
            return ServiceFactory.SysMenuPermission.MenuPermissionJson(menuId);
        }

        /// <summary>
        /// 提供权限类型json数据
        /// </summary>
        /// <returns></returns>
        [Description("提供权限类型json数据")]
        [Login]
        [CatchException(ActionType.数据)]
        public JsonResult PermissionTypeJson()
        {
            return ServiceFactory.SysMenuPermission.PermissionTypeJson();
        }

        /// <summary>
        /// 提供菜单功能点json数据
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns></returns>
        [Description("提供菜单功能点json数据")]
        [Login]
        [CatchException(ActionType.数据)]
        public JsonResult MenuFunctionJson1(int menuId)
        {
            return ServiceFactory.SysMenuFunction.MenuFunctionJson1(menuId);
        }

        #endregion

        #region 操作

        /// <summary>
        /// 保存菜单
        /// </summary>
        /// <param name="model">菜单模型</param>
        /// <returns></returns>
        [Description("保存菜单")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveMenu(MenuModel model)
        {
            return ServiceFactory.SysMenu.SaveMenu(model);
        }

        /// <summary>
        /// 移除菜单
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns></returns>
        [Description("移除菜单")]
        [CatchException(ActionType.操作)]
        public JsonResult RemoveMenu(int id)
        {
            return ServiceFactory.SysMenu.RemoveMenu(id);
        }

        /// <summary>
        /// 菜单功能点保存
        /// </summary>
        /// <param name="model">菜单功能点模型</param>
        /// <returns></returns>
        [Description("菜单功能点保存请求")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveMenuFunction(MenuFunctionModel model)
        {
            return ServiceFactory.SysMenuFunction.SaveMenuFunction(model);
        }

        /// <summary>
        /// 菜单功能点删除
        /// </summary>
        /// <param name="id">菜单功能点ID</param>
        /// <returns></returns>
        [Description("菜单功能点删除")]
        [CatchException(ActionType.操作)]
        public JsonResult RemoveMenuFunction(int id)
        {
            return ServiceFactory.SysMenuFunction.RemoveMenuFunction(id);
        }

        /// <summary>
        /// 菜单权限保存
        /// </summary>
        /// <param name="model">菜单权限模型</param>
        /// <returns></returns>
        [Description("菜单权限保存请求")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveMenuPermission(MenuPermissionModel model)
        {
            return ServiceFactory.SysMenuPermission.SaveMenuPermission(model);
        }

        /// <summary>
        /// 菜单权限删除
        /// </summary>
        /// <param name="id">菜单权限ID</param>
        /// <returns></returns>
        [Description("菜单权限删除")]
        [CatchException(ActionType.操作)]
        public JsonResult RemoveMenuPermission(int id)
        {
            return ServiceFactory.SysMenuPermission.RemoveMenuPermission(id);
        }

        #endregion

        #region 其他

        #endregion
    }
}
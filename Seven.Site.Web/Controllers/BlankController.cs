﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.IO;

using Seven.Site.Mvc.Attributes;
using Seven.Web.Http;
using Seven.Tools.Helper;

namespace Seven.Site.Web.Controllers
{
    public class BlankController : Controller
    {
        #region 页面

        #endregion

        #region 数据

        #endregion

        #region 操作

        #endregion

        #region 其他

        #endregion

        #region 测试

        /// <summary>
        /// 提示页面
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        [Description("提示页面")]
        [AllowAnonymous]
        [ViewPage]
        public ViewResult Index(string msg = "")
        {
            ViewBag.Msg = Server.UrlDecode(msg);

            return View();
        }

        /// <summary>
        /// 测试选择器页面
        /// </summary>
        /// <returns></returns>
        [Description("测试选择器页面")]
        [AllowAnonymous]
        [ViewPage]
        public ViewResult Test1()
        {
            return View();
        }

        /// <summary>
        /// 测试无扩展页面
        /// </summary>
        /// <returns></returns>
        [Description("测试无扩展页面")]
        [AllowAnonymous]
        [ViewPage]
        public ViewResult ClearTest()
        {
            return View();
        }

        /// <summary>
        /// 测试页面
        /// </summary>
        /// <returns></returns>
        [Description("测试页面")]
        [AllowAnonymous]
        [ViewPage]
        public ViewResult TestForTabPanel(string mat = "No0")
        {
            ViewBag.mat = mat;
            return View();
        }

        #endregion

        public static List<string> md5s = new List<string>();

        /// <summary>
        /// 测试附件上传页面
        /// </summary>
        /// <returns></returns>
        [Description("测试附件上传页面")]
        [AllowAnonymous]
        [ViewPage]
        public ViewResult TestForUpload()
        {
            return View();
        }

        /// <summary>
        /// 测试附件上传
        /// </summary>
        /// <returns></returns>
        [Description("测试附件上传")]
        [AllowAnonymous]
        [HttpPost]
        public JsonResult UploadFile()
        {
            var files = this.HttpContext.Request.Files; string result = ""; long nextStartPosition = 0;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFileBase file = files[i];
                string savePath = this.HttpContext.Request.MapPath("/Upload");
                string path = Path.Combine(savePath, file.FileName);
                long fileLenth;
                nextStartPosition = new UploadProcess().SaveAs(path, savePath, file, out fileLenth);
                //this.HttpContext.Response.Write(JsonHelper.SerializeObject(new { name = file.FileName }));
                this.HttpContext.Response.StatusCode = (int)HttpStatusCode.OK;
                result = string.Format("bytes={0}-{1}/{2}", 0, nextStartPosition, fileLenth);
                this.HttpContext.Response.AddHeader("Range", result);
                this.HttpContext.Response.Expires = -1;
            }

            return Json(new { next = nextStartPosition });
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult UploadFile(string md5)
        {
            bool exist = false;
            if (!md5s.Contains(md5)) { md5s.Add(md5); } else { exist = true; }

            return Json(new { exist = exist }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult ClearStaticMd5()
        {
            md5s.Clear();
            return Json(new { success = true });
        }
    }
}
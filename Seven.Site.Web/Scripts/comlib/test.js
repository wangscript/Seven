﻿(function ($) {

    $.util.namespace("$.seven.test");

    //单个dg选择器-测试
    //参数说明：
    //       onEnter：“确定”按钮的触发事件，参数签名为row或rows，取决于该选择器是单选还是多选（由dataOptions中的singleSelect控制）
    //       selected：已选项的id串，逗号相隔，或者数组
    $.seven.test.showSingleDataGridSelector = function (onEnter, selected) {
        //生成查询按钮的id，为回车查询功能定位按钮
        var btnId = "btn" + $.util.guid("N");
        var toolbar = [
                { type: "label", options: { text: "姓名：" } },
                {
                    type: "textbox", options: {
                        name: "realName", width: 120,
                        keydown: function (event, t) {
                            if (event.keyCode == 13) {
                                $(t).find("#" + btnId).click();
                                event.preventDefault();
                            }
                        }
                    }
                },
                {
                    type: "button", options: {
                        text: "查询", iconCls: "icon-search", id: btnId,
                        onclick: function (t) {
                            window.jeasyui.helper.searchForSelector($(t), "SingleDataGrid");
                        }
                    }
                }
        ];

        var opts = {
            title: "测试选择",
            onEnter: onEnter,
            extToolbar: true,
            selected: selected,
            datagridOptions: {
                singleSelect: false,
                toolbar: toolbar,
                url: "/Public/StaffJsonForSelectorGrid",
                idField: 'ID',
                columns: [[
                    { field: 'ID', title: '工号', width: 120 },
                    { field: 'RealName', title: '姓名', width: 90 }
                ]],
                onBeforeLoad: function (param) {
                    return !param.first;
                }
            }
        };

        //dia是选择器的dialog对象，其中datagrid对象被放在dia对象的datagrid属性中
        var dia = $.easyui.showSingleDataGridSelector(opts);
    };


    //两个dg选择器-测试
    //参数说明：
    //       onEnter：“确定”按钮的触发事件，参数签名为row或rows，取决于该选择器是单选还是多选（由dataOptions中的singleSelect控制）
    //       selected：已选项的id串，逗号相隔，或者数组
    $.seven.test.showDblDataGridSelector = function (onEnter, selected) {
        var btnId = "btn" + $.util.guid("N");
        var toolbar = [
                { type: "label", options: { text: "姓名：" } },
                {
                    type: "textbox", options: {
                        name: "realName", width: 120, keydown: function (event, t) {
                            if (event.keyCode == 13) {
                                $(t).find("#" + btnId).click();
                                event.preventDefault();
                            }
                        }
                    }
                },
                {
                    type: "button", options: {
                        text: "查询", iconCls: "icon-search", id: btnId,
                        onclick: function (t) {
                            window.jeasyui.helper.searchForSelector($(t), "DblDataGrid");
                        }
                    }
                }
        ];

        var opts = {
            title: "测试选择",
            onEnter: onEnter,
            extToolbar: true,
            selected: selected,
            datagridOptions: {
                singleSelect: false,
                toolbar: toolbar,
                url: "/Public/StaffJsonForSelectorGrid",
                selectedUrl: "/Public/StaffJsonForSelectedGrid",
                //queryParams: { first: true },
                idField: 'ID',
                columns: [[
                    { field: 'ID', title: '工号', width: 120 },
                    { field: 'RealName', title: '姓名', width: 90 }
                ]],
                pagination: true,
                onBeforeLoad: function (param) {
                    return !param.first;
                },
                onSelect: function (rowIndex, rowData) {
                    console.warn("选择：" + rowData.RealName);
                },
                onUnselect: function (rowIndex, rowData) {
                    console.warn("取消选择：" + rowData.RealName);
                }
            }
        };

        var dia = $.easyui.showDblDataGridSelector(opts);
    };

    //一个tree选择器-测试
    //参数说明：
    //       onEnter：“确定”按钮的触发事件，参数签名为node或nodes，取决于该选择器是select选择还是check选择（由treeOptions中的checkbox控制）
    //       selected：已选项的id串，逗号相隔，或者数组
    //       removeId：要移除的node的id
    $.seven.test.showTreeSelector = function (onEnter, selected, removeId) {
        var opts = {
            onEnter: onEnter,
            selected: selected,
            treeOptions: {
                url: "/Public/FullMenusJson?showRoot=true",
                dataPlain: true,
                parentField: "ParentID",
                onLoadSuccess: function (node, data) {
                    if (removeId && data.length > 0) {
                        var tree = $(this);
                        if (String(removeId) != "0") {
                            var node = tree.tree("find", removeId);
                            if (node) { tree.tree("remove", node.target); }
                        }
                    }
                }
            }
        };

        var dia = $.easyui.showTreeSelector(opts);
    };


    //一个tree和两个dg选择器-测试
    //参数说明：
    //       onEnter：“确定”按钮的触发事件，参数签名为row或rows，取决于该选择器是单选还是多选（由dataOptions中的singleSelect控制）
    //       selected：已选项的id串，逗号相隔，或者数组
    $.seven.test.showTreeDblDataGridSelector = function (onEnter, selected) {
        var btnId = "btn" + $.util.guid("N"),
            onSelectParamBuild = function (node) {
                var param = {};
                if (node.attributes.IsCompany) { param = { companyId: node.id }; }
                else if (node.attributes.IsDept) { param = { deptId: node.id }; }
                return param;
            };
        var toolbar = [
                { type: "label", options: { text: "姓名：" } },
                {
                    type: "textbox", options: {
                        name: "realName", width: 120, keydown: function (event, t) {
                            if (event.keyCode == 13) {
                                $(t).find("#" + btnId).click();
                                event.preventDefault();
                            }
                        }
                    }
                },
                {
                    type: "button", options: {
                        text: "查询", iconCls: "icon-search", id: btnId,
                        onclick: function (t) {
                            window.jeasyui.helper.searchForSelector($(t), "TreeDblDataGrid");
                        }
                    }
                }
        ];

        var opts = {
            onEnter: onEnter,
            extToolbar: true,
            selected: selected,
            width: 900,
            treeWidth: 200,
            treeTitle: "组织结构列表",
            treeOptions: {
                url: "/Public/OrganizationJsonWithSelfForTree?id=0&flag=0",
                dataPlain: true,
                parentField: "ParentID",
                onSelectParamBuild: onSelectParamBuild
            },
            datagridOptions: {
                singleSelect: false,
                toolbar: toolbar,
                url: "/Public/StaffJsonForSelectorGrid",
                selectedUrl: "/Public/StaffJsonForSelectedGrid",
                queryParams: { first: true },
                idField: 'ID',
                columns: [[
                    { field: 'ID', title: '工号', width: 120 },
                    { field: 'RealName', title: '姓名', width: 90 }
                ]],
                pagination: true,
                onBeforeLoad: function (param) {
                    return !param.first;
                },
                onSelect: function (rowIndex, rowData) {
                    console.warn("选择：" + rowData.RealName);
                },
                onUnselect: function (rowIndex, rowData) {
                    console.warn("取消选择：" + rowData.RealName);
                }
            }
        };

        var dia = $.easyui.showTreeDblDataGridSelector(opts);
    };


    //一个Accordion和两个dg选择器-测试
    //参数说明：
    //       onEnter：“确定”按钮的触发事件，参数签名为row或rows，取决于该选择器是单选还是多选（由dataOptions中的singleSelect控制）
    //       selected：已选项的id串，逗号相隔，或者数组
    $.seven.test.showAccordionDblDataGridSelector = function (onEnter, selected) {
        var btnId = "btn" + $.util.guid("N"),
            onSelectParamBuild = function (node) {
                var param = {};
                if (node.attributes.IsCompany) { param = { companyId: node.id }; }
                else if (node.attributes.IsDept) { param = { deptId: node.id }; }
                return param;
            };
        var toolbar = [
                { type: "label", options: { text: "姓名：" } },
                {
                    type: "textbox", options: {
                        name: "realName", width: 120, keydown: function (event, t) {
                            if (event.keyCode == 13) {
                                $(t).find("#" + btnId).click();
                                event.preventDefault();
                            }
                        }
                    }
                },
                {
                    type: "button", options: {
                        text: "查询", iconCls: "icon-search", id: btnId,
                        onclick: function (t) {
                            window.jeasyui.helper.searchForSelector($(t), "AccordionDblDataGrid");
                        }
                    }
                }
        ];
        var opts = {
            onEnter: onEnter,
            extToolbar: true,
            selected: selected,
            width: 900,
            accordionOptions: {
                width: 170,
                panels: [
                    {
                        panelTitle: "组织结构列表", type: "tree", typeOptions: {
                            url: "/Public/OrganizationJsonWithSelfForTree?id=0&flag=0",
                            dataPlain: true,
                            parentField: "ParentID",
                            onSelectParamBuild: onSelectParamBuild
                        }
                    },
                    { panelTitle: "文本测试", type: "", typeOptions: "测试文本测试文本测试文本测试文本测试文本测试文本" },
                ]
            },
            datagridOptions: {
                singleSelect: false,
                toolbar: toolbar,
                url: "/Public/StaffJsonForSelectorGrid",
                selectedUrl: "/Public/StaffJsonForSelectedGrid",
                queryParams: { first: true },
                idField: 'ID',
                columns: [[
                    { field: 'ID', title: '工号', width: 120 },
                    { field: 'RealName', title: '姓名', width: 90 }
                ]],
                pagination: true,
                onBeforeLoad: function (param) {
                    return !param.first;
                }
            }
        };

        var dia = $.easyui.showAccordionDblDataGridSelector(opts);
    }

})(jQuery)
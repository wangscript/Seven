﻿(function ($) {

    $.util.namespace("mainpage.authmenus");

    window.mainpage.authmenus.getRootMenus = function (callback) {
        $.post("/Public/GetRootMenusJson", function (data, textStatus, XMLHttpRequest) {
            if ($.isFunction(callback)) { callback.call(this, data); }
        });
    };

    window.mainpage.authmenus.getChildrenMenus = function (id, code, callback) {
        $.post("/Public/GetChildrenMenusJson", { parentId: id, parentCode: code }, function (data, textStatus, XMLHttpRequest) {
            if ($.isFunction(callback)) { callback.call(this, data); }
        });
    };

})(jQuery);
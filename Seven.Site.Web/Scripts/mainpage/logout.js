﻿(function ($) {

    $.util.namespace("mainpage");

    window.mainpage.loginPage = window.comlib.loginPageUrl;
    window.mainpage.logoutPage = "/Account/Logout";

    //点击注销动作，确认用户是否注销，如果是则注销当前登录并且返回登录界面。
    window.mainpage.logoutConfirmAndExit = function (callback, callbackIn) {
        $.messager.confirm("操作提醒", "您确定要注销本次登录？", function (q) {
            if (q) {
                window.mainpage.logoutAndExit(callback, callbackIn);
            }
        });
    };

    //点击退出系统动作，确认用户是否退出系统，如果是则注销当前登录并且关闭当前窗口。
    window.mainpage.exitSysConfirm = function () {
        $.messager.confirm("操作提醒", "您确定要退出系统并关闭本窗口？", function (q) {
            if (q) {
                window.mainpage.logout($.util.closeWindowNoConfirm);
            }
        });
    };

    //系统注销登录并且返回登录界面。
    window.mainpage.logoutAndExit = function (callback, callbackIn) {
        if ($.isFunction(callback)) { callback.call(this); }
        window.mainpage.logout(function (data, textStatus, XMLHttpRequest) {
            if ($.isFunction(callbackIn)) { callbackIn.call(this); }
            var id = "span_" + $.util.guid("N");
            var span = "#" + id;
            var timeout = 5;
            var jump = function () {
                window.location.href = window.mainpage.loginPage;
            };
            $.messager.alert("操作提醒", "注销成功，系统将会在您点击本窗口的 \"确定\"/\"关闭\" 按钮或者 <span id='" + id + "'>" + timeout + "</span> 秒后跳转到登录页面", "info");
            $(span).currentPanel().panel("options").onClose = jump;
            var c = function (value) { $(span).text(value); if (value == 0) { jump(); } };
            var run = function () {
                c(timeout);
                timeout--;
                if (timeout >= 0) { $.util.exec(run, 1000); }
            };
            $.util.exec(run);
        });
    };

    //系统注销登录，并在注销后执行一个回调函数。
    window.mainpage.logout = function (callback) {
        $.post(window.mainpage.logoutPage, function (data, textStatus, XMLHttpRequest) {
            $.easyui.loaded($("body"));
            if (data.success) {
                if ($.util.isFunction(callback)) { callback.apply(this, arguments); }
            }
            else {
                $.messager.alert("操作提醒", unescape(data.message), "info");
            }
        });
    }

})(jQuery);
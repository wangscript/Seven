﻿(function ($) {

    $.util.namespace("jeasyui.helper");

    //对指定范围内的form进行校验，若校验失败，则返回null；若校验通过，则返回form的序列化结果
    //dialog：指定范围jq对象 或者 目标form的jq对象
    //param：序列化操作的相关参数，详见form.js
    window.jeasyui.helper.formValidate = function (dialog, param) {
        var verifyResult = false;
        if (/^(?:form)$/i.test(dialog[0].nodeName)) {
            verifyResult = dialog.form("validate");
        }
        else {
            verifyResult = $("form", dialog).form("validate");
        }
        if (!verifyResult) { return null };
        var model = param == undefined ? $(dialog).form('getData') : $(dialog).form('getData', param);
        return model;
    };

    //action执行后的回调函数
    //result：一个object对象，固定格式是{ success : true, message : "返回信息" }
    //callback：一个回调函数，在执行成功后调用
    //showSuccessMsg：是否显示成功执行的信息提示，默认为false
    window.jeasyui.helper.actionDoneCallBack = function (result, callback, showSuccessMsg) {
        showSuccessMsg = showSuccessMsg != undefined ? $.string.toBoolean(String(showSuccessMsg)) : false;
        if (result.success) {
            if (showSuccessMsg) { $.easyui.messager.show("操作成功。"); }
            if ($.isFunction(callback))
            { callback.call(this); }
        } else {
            $.easyui.messager.show({ msg: result.message == undefined ? "操作失败。" : result.message, iconCls: "error", title: "错误提醒" });
        }
    };

    //新增、更新action执行后需要操作TreeGrid组件的回调函数，对TreeGrid组件的操作包含：追加node，更新node，移动node，并选中新的node
    //result：一个object对象，包含以下3个属性
    //        success : Boolen值，true时提示操作成功并执行内置的对TreeGrid的操作函数，false时提示返回信息
    //        message : 提示信息
    //        data: Object对象，该对象来自ServiceResult的定义中，并且该对象需包含以下2个属性
    //            -->  state : string值，可以是 insert 或者 update 或者 move。
    //                         为insert时，对TreeGrid的操作将是追加node，为update时，对TreeGrid的操作将是更新node，
    //                         为move时，对TreeGrid的操作将是移动并更新node
    //            -->  newObj：新增或者更新后返回的新的操作数据对象，必须符合该TreeGrid在options中设置的数据格式，
    //                         包含idField，treeField，parentField，codeField，iconCls，[state]，[checked]等属性
    //treegrid：TreeGrid对象，可以是jq对象，也可以是dom对象
    //showSuccessMsg：是否显示成功执行的信息提示，默认为false
    //注，根据TreeGrid的数据提供方式（同步或异步），对node将有不同的操作方式。
    //    请设置parentField值（默认为ParentID），也需要在newObj对象中提供该Field的值，该Field用来判定父子关系
    //    请设置codeField值（默认为Code），也需要在newObj对象中提供该Field的值，该Field用来“异步时定位node”。若TreeGrid是同步的，可以不提供codeField
    window.jeasyui.helper.actionDoneCallBackForTreeGrid = function (result, treegrid, showSuccessMsg) {
        showSuccessMsg = showSuccessMsg != undefined ? $.string.toBoolean(String(showSuccessMsg)) : false;
        var _callback = function (info) {
            treegrid = $(treegrid);
            var newNode = info.newObj;
            var opts = treegrid.treegrid("options"), url = opts.url, idField = opts.idField, treeField = opts.treeField, parentField = opts.parentField ? opts.parentField : "ParentID", codeField = "Code";
            if (!newNode[parentField]) { newNode[parentField] = 0; }

            if (info.state == "insert") {
                var parentNode = treegrid.treegrid("findRow", newNode[parentField]);
                if (parentNode) {
                    //是否叶子节点
                    //是，直接append
                    //否，找children，
                    //      存在children，append之后再expandTo，最后select
                    //      不存在children，执行url获取子节点，将子节点append，再find新节点，expandTo后select
                    //  这种判定方式有问题：一个，折叠的，且未加载子节点数据的node，他的isLeaf返回值是true。。。
                    //var test = treegrid.treegrid("isLeaf", parentNode.target);
                    //if (test) {
                    //    //找得到父节点，且父节点“是叶子节点”，append到下面后再select
                    //    newNode.Name += "父级是否叶子：" + test + ",父级名：" + parentNode.Name;
                    //    treegrid.treegrid('append', {
                    //        parent: parentNode[idField],
                    //        data: [newNode]
                    //    });
                    //    treegrid.treegrid("select", newNode[idField]);
                    //}
                    //else {
                    //    var brotherNodes = treegrid.treegrid("getChildren", parentNode[idField]);
                    //    if (brotherNodes.length == 0) {
                    //        //找得到父节点，且父节点“不是叶子节点”并从未加载子节点，获取子节点append后再select
                    //        $.post(url, { id: parentNode[idField] }, function (result) {
                    //            treegrid.treegrid("append", {
                    //                parent: parentNode[idField],
                    //                data: result
                    //            });
                    //            var tempNode = treegrid.treegrid("findRow", newNode[idField]);
                    //            if (tempNode) {
                    //                treegrid.treegrid("expandTo", tempNode[idField]);
                    //                treegrid.treegrid("select", tempNode[idField]);
                    //            }
                    //        });
                    //    }
                    //    else {
                    //        //找得到父节点，且父节点已加载子节点，append到下面后expandTo、select
                    //        treegrid.treegrid('append', {
                    //            parent: parentNode[idField],
                    //            data: [newNode]
                    //        });
                    //        treegrid.treegrid("expandTo", newNode[idField]);
                    //        treegrid.treegrid("select", newNode[idField]);
                    //    }
                    //}

                    var brotherNodes = treegrid.treegrid("getChildren", parentNode[idField]);
                    if (brotherNodes.length == 0) {
                        if (parentNode.state == "closed") {
                            //找得到父节点，且父节点“折叠”并从未加载子节点，获取子节点append后再select
                            $.post(url, { id: parentNode[idField] }, function (result) {
                                treegrid.treegrid("append", {
                                    parent: parentNode[idField],
                                    data: result
                                });
                                var tempNode = treegrid.treegrid("findRow", newNode[idField]);
                                if (tempNode) {
                                    treegrid.treegrid("expandTo", tempNode[idField]);
                                    treegrid.treegrid("select", tempNode[idField]);
                                }
                            });
                        }
                        else {
                            //找得到父节点，且父节点“展开”，append到下面后再select
                            treegrid.treegrid('append', {
                                parent: parentNode[idField],
                                data: [newNode]
                            });
                            treegrid.treegrid("select", newNode[idField]);
                        }
                    }
                    else {
                        //找得到父节点，且父节点已加载子节点，append到下面后expandTo、select
                        treegrid.treegrid('append', {
                            parent: parentNode[idField],
                            data: [newNode]
                        });
                        treegrid.treegrid("expandTo", newNode[idField]);
                        treegrid.treegrid("select", newNode[idField]);
                    }
                }
                else {
                    if (newNode[parentField] == 0) {
                        //找不到父节点，且父节点是0，append到root的最后再select
                        treegrid.treegrid('append', {
                            data: [newNode]
                        });
                        treegrid.treegrid("select", newNode[idField]);
                    }
                    else {
                        //找不到父节点，但父节点是正常的值，尝试根据Code属性进行定位
                        if (!$.string.isNullOrWhiteSpace(newNode[codeField])) {
                            //在father的子节点中查找child[codeField] 等于 targetCode的节点，找到返回该节点，找不到返回null
                            var getTargetNode = function (father, targetCode) {
                                var son = treegrid.treegrid("getChildren", father[idField]);
                                if (son.length == 0) {
                                    if (father.state == "closed") {
                                        $.ajax({
                                            async: false,
                                            type: "post",
                                            url: url,
                                            data: { id: father[idField] },
                                            success: function (data, textStatus, XMLHttpRequest) {
                                                if (data.length > 0) {
                                                    treegrid.treegrid("append", {
                                                        parent: father[idField],
                                                        data: data
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            };

                            var tempNode = undefined;
                            for (var k = 1; k <= (newNode[codeField].length / 2) - 1 ; k++) {
                                var prefix = newNode[codeField].substring(0, 2 * k);
                                tempNode = treegrid.treegrid("findRow", function (row) { return row[codeField] == prefix });
                                if (tempNode) {
                                    getTargetNode(tempNode, newNode[codeField].substring(0, 2 * (k + 1)));
                                }
                            }
                            var targetNode = treegrid.treegrid("findRow", newNode[idField]);
                            if (targetNode) {
                                treegrid.treegrid("expandTo", targetNode[idField]);
                                treegrid.treegrid("select", targetNode[idField]);
                            }
                        }
                    }
                }
            }
            else if (info.state == "update") {
                var oldNode = treegrid.treegrid("findRow", newNode[idField]);
                if (oldNode) {
                    treegrid.treegrid('update', {
                        id: oldNode[idField],
                        row: newNode
                    });
                }
            }
            else if (info.state == "move") {
                //先更新自己
                var oldNode = treegrid.treegrid("findRow", newNode[idField]);
                if (oldNode) {
                    treegrid.treegrid('update', {
                        id: oldNode[idField],
                        row: newNode
                    });
                }
                //再更新子节点code
                var childrenNodes = treegrid.treegrid("getChildren", oldNode[idField]);
                if (childrenNodes.length > 0) {
                    $.post(url, { id: oldNode[idField] }, function (result) {
                        result.forEach(function (node) {
                            var tempNode = $.array.first(childrenNodes, function (item) { return item[idField] == node[idField] });
                            if (tempNode) {
                                tempNode[codeField] = node[codeField];
                                treegrid.treegrid('update', {
                                    id: tempNode[idField],
                                    row: tempNode
                                });
                            }
                        });
                        //再移动
                        var popNode = treegrid.treegrid("pop", oldNode[idField]);
                        treegrid.treegrid("append", {
                            parent: popNode[parentField],
                            data: [popNode]
                        });
                    });
                }
                else {
                    //再移动
                    var popNode = treegrid.treegrid("pop", oldNode[idField]);
                    treegrid.treegrid("append", {
                        parent: popNode[parentField],
                        data: [popNode]
                    });
                }
            }
        }
        if (result.success) {
            if (showSuccessMsg) { $.easyui.messager.show("操作成功。"); }
            _callback(result.data);
        } else {
            $.easyui.messager.show({ msg: result.message == undefined ? "操作失败。" : result.message, iconCls: "error", title: "错误提醒" });
        }
    };


    //移除action执行后的回调函数 移除成功，执行回调函数；移除失败，且移除目标已不存在，执行回调函数并提示返回信息；其他，提示返回信息
    //result：一个object对象，固定格式是{ success : true, remove : true, msg : "返回信息" }
    //        success:必须提供。true时，调用回调函数；false时，若remove为true，调用回调函数；
    //        remove:可选。true时表示操作对象在数据库中已不存在，调用回调函数；
    //        msg:提示信息。
    //callback：一个回调函数
    //showSuccessMsg：是否显示成功执行的信息提示，默认为false
    window.jeasyui.helper.actionDoneCallBackWhenRemove = function (result, callback, showSuccessMsg) {
        showSuccessMsg = showSuccessMsg != undefined ? $.string.toBoolean(String(showSuccessMsg)) : false;
        if (result.success) {
            if (showSuccessMsg) { $.easyui.messager.show("操作成功。"); }
            if ($.isFunction(callback))
            { callback.call(this); }
        } else {
            if (result.remove) {
                if ($.isFunction(callback))
                { callback.call(this); }
            }
            else {
                $.easyui.messager.show({ msg: result.message == undefined ? "操作失败。" : result.message, iconCls: "error", title: "错误提醒" });
            }
        }
    };

    //新增、更新action执行后需要操作Tree组件的回调函数，对Tree组件的操作包含：追加node，更新node，移动node，并选中新的node
    //result：一个object对象，包含以下3个属性
    //        success : Boolen值，true时提示操作成功并执行内置的对Tree的操作函数，false时提示返回信息
    //        message : 提示信息
    //        data: Object对象，该对象来自ServiceResult的定义中，并且该对象需包含以下2个属性
    //            -->  state : string值，可以是 insert 或者 update 或者 move。
    //                         为insert时，对Tree的操作将是追加node，为update时，对TreeGrid的操作将是更新node，
    //                         为move时，对Tree的操作将是移动并更新node
    //            -->  newObj：新增或者更新后返回的新的操作数据对象，必须符合该Tree在options中设置的数据格式，
    //                         包含idField，treeField，parentField，codeField，iconCls，[state]，[checked]等属性
    //tree：Tree对象，可以是jq对象，也可以是dom对象
    //showSuccessMsg：是否显示成功执行的信息提示，默认为false
    //注，根据Tree的数据提供方式（同步或异步），对node将有不同的操作方式。
    //    请设置parentField值（默认为ParentID），也需要在newObj对象中提供该Field的值，该Field用来判定父子关系
    //    请设置codeField值（默认为Code），也需要在newObj对象中提供该Field的值，该Field用来“异步时定位node”。若Tree是同步的，可以不提供codeField
    window.jeasyui.helper.actionDoneCallBackForTree = function (result, tree, showSuccessMsg) {
        showSuccessMsg = showSuccessMsg != undefined ? $.string.toBoolean(String(showSuccessMsg)) : false;
        var _callback = function (info) {
            tree = $(tree);
            var newNode = info.newObj; if (!newNode.attributes) { newNode.attributes = {}; }
            var opts = tree.tree("options"), url = opts.url, idField = "id", parentField = opts.parentField ? opts.parentField : "ParentID", codeField = opts.codeField ? opts.codeField : "Code";
            if (!newNode.attributes[parentField]) { newNode.attributes[parentField] = 0; }
            var parentValue = newNode.attributes[parentField];

            if (info.state == "insert") {
                var parentNode = tree.tree("find", parentValue);
                if (parentNode) {
                    var brotherNodes = tree.tree("getChildren", parentNode.target);
                    if (brotherNodes.length == 0) {
                        if (parentNode.state == "closed") {
                            //找得到父节点，且父节点“折叠”并从未加载子节点，获取子节点append后再select
                            $.post(url, { id: parentNode[idField] }, function (result) {
                                tree.tree("append", {
                                    parent: parentNode.target,
                                    data: result
                                });
                                var tempNode = tree.tree("find", newNode[idField]);
                                if (tempNode) {
                                    tree.tree("expandTo", tempNode.target);
                                    tree.tree("select", tempNode.target);
                                }
                            });
                        }
                        else {
                            //找得到父节点，且父节点“展开”，append到下面后再select
                            tree.tree('append', {
                                parent: parentNode.target,
                                data: [newNode]
                            });
                            var tempNode = tree.tree("find", newNode[idField]);
                            if (tempNode) {
                                tree.tree("select", tempNode.target);
                            }
                        }
                    }
                    else {
                        //找得到父节点，且父节点已加载子节点，append到下面后expandTo、select
                        tree.tree('append', {
                            parent: parentNode.target,
                            data: [newNode]
                        });
                        var tempNode = tree.tree("find", newNode[idField]);
                        if (tempNode) {
                            tree.tree("expandTo", newNode.target);
                            tree.tree("select", newNode.target);
                        }
                    }
                }
                else {
                    if (parentValue == 0) {
                        //找不到父节点，且父节点是0，append到root的最后再select
                        tree.tree('append', {
                            data: [newNode]
                        });
                        var tempNode = tree.tree("find", newNode[idField]);
                        if (tempNode) {
                            tree.tree("select", tempNode.target);
                        }
                    }
                    else {
                        //找不到父节点，但父节点是正常的值，尝试根据attributes属性中的Code属性进行定位
                        if (newNode.attributes && !$.string.isNullOrWhiteSpace(newNode.attributes[codeField])) {
                            //在father的子节点中查找child.attributes[codeField] 等于 targetCode的节点，找到返回该节点，找不到返回null
                            var tempCode = newNode.attributes[codeField], len = tempCode.length;
                            var getTargetNode = function (father, targetCode) {
                                var son = tree.tree("getChildren", father.target);
                                if (son.length == 0) {
                                    if (father.state == "closed") {
                                        $.ajax({
                                            async: false,
                                            type: "post",
                                            url: url,
                                            data: { id: father[idField] },
                                            success: function (data, textStatus, XMLHttpRequest) {
                                                if (data.length > 0) {
                                                    tree.tree("append", {
                                                        parent: father.target,
                                                        data: data
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            };

                            var tempNode = undefined, allNodes = tree.tree("getNodes");
                            for (var k = 1; k <= (len / 2) - 1 ; k++) {
                                var prefix = tempCode.substring(0, 2 * k);
                                tempNode = $.array.first(allNodes, function (node) { return node.attributes[codeField] == prefix; });
                                if (tempNode) {
                                    getTargetNode(tempNode, tempCode.substring(0, 2 * (k + 1)));
                                }
                            }
                            var targetNode = tree.tree("find", newNode[idField]);
                            if (targetNode) {
                                tree.tree("expandTo", targetNode.target);
                                tree.tree("select", targetNode.target);
                            }
                        }
                    }
                }
            }
            else if (info.state == "update") {
                var oldNode = tree.tree("find", newNode[idField]);
                if (oldNode) {
                    tree.tree('update', {
                        target: oldNode.target,
                        text: newNode.text,
                        iconCls: newNode.iconCls,
                        checked: newNode.checked,
                        attributes: newnode.attributes
                    });
                }
            }
            else if (info.state == "move") {
                var oldNode = tree.tree("find", newNode[idField]);
                if (oldNode) {
                    //先更新自己
                    tree.tree('update', {
                        target: oldNode.target,
                        text: newNode.text,
                        iconCls: newNode.iconCls,
                        checked: newNode.checked,
                        attributes: newNode.attributes
                    });

                    var childrenNodes = tree.tree("getChildren", oldNode.target);
                    if (childrenNodes.length > 0) {
                        //若子节点存在，并且code属性有效，就更新子节点的code
                        if (!$.string.isNullOrWhiteSpace(newNode.attributes[codeField])) {
                            $.post(url, { id: oldNode[idField] }, function (result) {
                                result.forEach(function (node) {
                                    var tempNode = $.array.first(childrenNodes, function (item) { return item[idField] == node[idField] });
                                    if (tempNode) {
                                        tempNode.attributes[codeField] = node.attributes[codeField];
                                        tree.tree('update', {
                                            target: tempNode.target,
                                            attributes: tempNode.attributes
                                        });
                                    }
                                });
                                //再移动至新节点的父节点下
                                var parentNode = tree.tree("getParent", oldNode.target);
                                if (parentNode) {
                                    var popNode = tree.tree("pop", oldNode.target);
                                    tree.tree("append", {
                                        parent: parentNode.target,
                                        data: [popNode]
                                    });
                                }
                            });
                        }
                    }
                    else {
                        //再移动至新节点的父节点下
                        var parentNode = tree.tree("getParent", oldNode.target);
                        if (parentNode) {
                            var popNode = tree.tree("pop", oldNode.target);
                            tree.tree("append", {
                                parent: parentNode.target,
                                data: [popNode]
                            });
                        }
                    }
                }
            }
        }

        if (result.success) {
            if (showSuccessMsg) { $.easyui.messager.show("操作成功。"); }
            _callback(result.data);
        } else {
            $.easyui.messager.show({ msg: result.message == undefined ? "操作失败。" : result.message, iconCls: "error", title: "错误提醒" });
        }
    };



    //action执行后继续保存附件的回调函数
    //form：表单jq对象，用来限定选择器的范围
    //key：表单中主键ID的字段名
    //prefix：附件相关控件的前缀
    //result：一个object对象，固定格式是{ success : true, key : 主键ID值, msg : "返回信息" }
    //callback：一个回调函数，在执行成功后调用
    //showSuccessMsg：是否显示成功执行的信息提示，默认为true
    //window.jeasyui.helper.actionDoneCallBackAttachment = function (form, key, prefix, result, callback, showSuccessMsg) {
    //    showSuccessMsg = showSuccessMsg != undefined ? $.string.toBoolean(String(showSuccessMsg)) : true;
    //    if (result.success) {
    //        var keyControl = $("#" + key, form);
    //        var uploadControl = $("#" + prefix + "_uploadfiles", form);
    //        var completeControl = $("#" + prefix + "_hasCompleted", form);
    //        keyControl.val(result.key);
    //        uploadControl.click();
    //        var _run = function () {
    //            if (!$.string.toBoolean(completeControl.val())) {
    //                $.util.exec(_run, 500);
    //            }
    //            else {
    //                if (showSuccessMsg) { $.easyui.messager.show("操作成功。"); }
    //                if ($.isFunction(callback))
    //                { callback.call(this); }
    //            }
    //        };
    //        $.util.exec(_run);
    //    } else {
    //        $.easyui.messager.show({ msg: result.msg, iconCls: "error", title: "错误提醒" });
    //    }
    //};

    //action执行后的回调函数 移除成功，执行回调函数；移除失败，且移除目标已不存在，执行回调函数并提示返回信息；其他，提示返回信息
    //result：一个object对象，固定格式是{ success : true, 未知属性 : true, msg : "返回信息" }
    //        success:必须提供。true时，若未知属性为true，提示操作成功，且调用回调函数；
    //                          true时，若未知属性为false，则提示返回信息，且调用回调函数；
    //                          false时，只提示返回信息；
    //        未知属性:可选。仅在success属性为true时生效。当success为true，若未知属性值为false时，将提示返回信息，同时调用回调函数；若未知属性值为true时，将提示操作成功，同时调用回调函数；
    //        msg:必须提供。
    //callback：一个回调函数
    //showSuccessMsg：是否显示成功执行的信息提示，默认为true
    //window.jeasyui.helper.actionDoneCallBackWithSecondProe = function (result, callback, showSuccessMsg) {
    //    showSuccessMsg = showSuccessMsg != undefined ? $.string.toBoolean(String(showSuccessMsg)) : true;
    //    if (result.success) {

    //        var secondProe = "none";
    //        var k = 0;
    //        for (var p in result) {
    //            if (k == 1) { secondProe = String(p); break; }
    //            k++;
    //        }

    //        if (result[secondProe]) {
    //            if (showSuccessMsg) { $.easyui.messager.show("操作成功。"); }
    //        }
    //        else {
    //            $.easyui.messager.show({ msg: result.msg, iconCls: "error", title: "错误提醒" });
    //        }
    //        if ($.isFunction(callback))
    //        { callback.call(this); }
    //    } else {
    //        $.easyui.messager.show({ msg: result.msg, iconCls: "error", title: "错误提醒" });
    //    }
    //};

    //弹出确认信息框，确认后执行回调函数
    //confirmMsg：确认信息
    //callBack：确认后执行的回调函数
    window.jeasyui.helper.confirmCallBack = function (confirmMsg, callBack) {
        $.messager.confirm('确认', confirmMsg, function (r) {
            if (r) {
                if ($.isFunction(callBack)) {
                    callBack.call(this);
                }
            }
        });
    }

    //弹出确认信息框，确认后执行回调函数。
    //该方法弹出一个包含三个按钮("是"、"否" 和 "取消")的对话框，点击任意按钮或者关闭对话框时，执行指定的回调函数；
    //
    //confirmMsg：确认信息
    //callBack：确认后执行的回调函数，参数签名为(r)，r的值在不同操作下的值分贝是：取消 时r为undefined，是 时r为true，否 时r为false
    window.jeasyui.helper.solicitCallBack = function (confirmMsg, callBack) {
        $.messager.solicit('操作提醒', confirmMsg, function (r) {
            if ($.isFunction(callBack)) {
                callBack.call(this, r);
            }
        });
    }

    //为datagrid的tools画一条top位置的border
    //datagrid：datagrid的jq对象
    //topBorderWidth：top位置的border的宽度，若不提供，则默认1px
    window.jeasyui.helper.drawTopBorderForDataGridTools = function (datagrid, topBorderWidth) {
        var panelSelector = datagrid.datagrid("getPanel");
        if (panelSelector.length != 0) {
            var toolbarSelector = $(panelSelector).children(".datagrid-toolbar:first");
            if (toolbarSelector.length != 0) {
                topBorderWidth = topBorderWidth || "1";
                toolbarSelector.css({ "border-top-width": "" + topBorderWidth + "px" });
            }
        }
    };

    //根据json格式的string转换成js对象后，解析成toolbar的data对象
    //json：string值,表示要转换的json格式的string对象
    //lastSplit：boolean值，表示是否在toolbar的data对象的最后加上“分隔符”
    //groupSplit：boolean值，表示是否在解析toolbar的data对象的过程中按分组标记分离。默认false，若true，则最终返回的data对象，每个元素都表示一个{ group:xxx,data:toolbar }格式的按分组标记分离的toolbar的data对象
    window.jeasyui.helper.buildToolbarUsePermissionJson = function (json, lastSplit, groupSplit) {
        var obj = [], toolbar = [];
        if ($.util.isString(json)) { obj = json.replaceAll("&quot;", "\"").toObject(); }
        if (groupSplit == true) {
            //分组
            var groupMarks = $.array.map(obj, function (val) { return val.GroupMark }).distinct();
            $.each(groupMarks, function (i, g) {
                //找到该分组名的对象集合
                var temp = $.array.filter(obj, function (val) { return val.GroupMark == g });
                if (temp.length > 0) {
                    var tempToolbar = [];
                    $.each(temp, function (k, p) {
                        if (k > 0) { tempToolbar.push("-"); }
                        var callback; var str = p.HandlerName;
                        if ($.string.isNullOrWhiteSpace(str)) { callback = function (t) { }; }
                        else {
                            if (!str.startsWith("function")) {
                                str = "function(t){" + str + "}";
                            }
                            callback = $.string.toFunction(str);
                        }
                        tempToolbar.push({ type: "button", options: { text: p.ButtonName, iconCls: p.IconCls, onclick: callback, disabled: p.IsDisabled } });
                    });
                    toolbar.push({ group: g, data: tempToolbar });
                }
            });
        }
        else {
            //不分组
            if (obj.length && obj.length > 0) {
                $.each(obj, function (i, p) {
                    if (i > 0) { toolbar.push("-"); }
                    var callback; var str = p.HandlerName;
                    if ($.string.isNullOrWhiteSpace(str)) { callback = function (t) { }; }
                    else {
                        if (!str.startsWith("function")) {
                            str = "function(t){" + str + "}";
                        }
                        callback = $.string.toFunction(str);
                    }
                    toolbar.push({ type: "button", options: { text: p.ButtonName, iconCls: p.IconCls, onclick: callback, disabled: p.IsDisabled } });
                });
            }
            if (toolbar.length > 0 && lastSplit) { toolbar.push("-"); }
        }
        return toolbar;
    };

    //将data中的分组名为group的那部分数据取出来
    //data：toolbar的data对象
    //group：分组名
    window.jeasyui.helper.splitToolbar = function (data, group) {
        var result = [];

        var temp = $.array.first(data, function (val) { return val.group && val.group.toLowerCase() == group; });
        if (temp && temp.data && $.array.isArray(temp.data)) { result = temp.data; }

        return result;
    };


    //从自封装选择器中获取指定的datagrid的jq对象
    //toolbar：工具条的jq对象
    //selectorType：选择器类型，string格式，可以是以下值：SingleDataGrid、DblDataGrid、TreeDblDataGrid、AccordionDblDataGrid
    window.jeasyui.helper.getDataGridFromSelector = function (toolbar, selectorType) {
        var dg = undefined;
        switch (selectorType) {
            case "SingleDataGrid":
                dg = toolbar.currentDatagrid();
                break;
            case "DblDataGrid":
                dg = toolbar.currentDatagrid();
                break;
            case "TreeDblDataGrid":
                dg = toolbar.currentDatagrid();
                break;
            case "AccordionDblDataGrid":
                dg = toolbar.currentDatagrid();
                break;
        }

        return dg;
    }

    //从含accordion的selector中获取参数对象
    window.jeasyui.helper.buildParamForAccordionSelector = function (toolbar) {
        var param = {}, accordion = undefined;

        var dia = toolbar.currentDialog(), diaPanel = dia.dialog("contentPanel"), layout = diaPanel.find("div.layout:eq(0)"),
                    leftPanel = layout.layout("panel", "west"), temp = leftPanel.find("div.accordion");
        if (temp.length > 0) { accordion = temp.first(); }
        if (temp && temp.isEasyUI("accordion")) {
            var accordionOpts = accordion.accordion("options"), multiple = accordionOpts.multiple, selectedPanels = [];
            if (multiple) {
                //accordion可同时选中多个panel，暂不考虑这种情况，若考虑的话，还得考虑tree的select事件，过于复杂。
                //var panels = accordion.accordion("getSelections");
                //if (panels && panels.length > 0) { $.array.copy(selectedPanels, panels); }
            }
            else {
                //accordion同时只能选中一个panel
                var panel = accordion.accordion("getSelected");
                if (panel) {
                    selectedPanels.push(panel);
                }
            }
            selectedPanels.forEach(function (p, i) {
                var panelOpts = p.panel("options");
                //queryable为true时表示该panel存在查询条件
                if (panelOpts.queryable == true) {
                    //queryType表示查询条件在哪
                    switch (panelOpts.queryType) {
                        case "tree":
                            var tree = p.find("ul.tree");
                            if (tree.length == 1 && tree.isEasyUI("tree")) {
                                //onSelectParamBuild表示查询条件怎么组装
                                var treeOpts = tree.tree("options"), builder = treeOpts.onSelectParamBuild, node = tree.tree("getSelected");
                                if (node) { $.extend(param, builder(node)); }
                            }
                            break;
                    }
                }
            });
        }

        return param;
    };

    //获取selector的查询参数
    window.jeasyui.helper.getSelectorParam = function (toolbar, selectorType) {
        var param = toolbar.toolbar("getValues");
        switch (selectorType) {
            case "SingleDataGrid":
            case "DblDataGrid":
                break;
            case "TreeDblDataGrid":
                var tree = undefined;
                var dia = toolbar.currentDialog(), diaPanel = dia.dialog("contentPanel"), layout = diaPanel.find("div.layout:eq(0)"),
                    leftPanel = layout.layout("panel", "west"), temp = leftPanel.find("ul.tree");
                if (temp.length > 0 && temp.isEasyUI("tree")) { tree = temp.first(); }

                if (tree) {
                    var treeOpts = tree.tree("options"), builder = treeOpts.onSelectParamBuild, node = tree.tree("getSelected");
                    if (node) { $.extend(param, builder(node)); }
                }

                break;
            case "AccordionDblDataGrid":
                $.extend(param, window.jeasyui.helper.buildParamForAccordionSelector(toolbar));
                break;
        }
        return param;
    };

    //为selector执行查询动作
    //toolbar：工具条的jq对象
    //selectorType：选择器类型，string格式，可以是以下值：SingleDataGrid、DblDataGrid、TreeDblDataGrid、AccordionDblDataGrid
    //otherParam：额外的参数对象
    window.jeasyui.helper.searchForSelector = function (toolbar, selectorType, otherParam) {
        var param = window.jeasyui.helper.getSelectorParam(toolbar, selectorType);
        var dg = window.jeasyui.helper.getDataGridFromSelector(toolbar, selectorType);
        if (dg) {
            if (otherParam == undefined) { dg.datagrid("load", param); }
            else { dg.datagrid("load", $.extend(otherParam, param)); }
        }
    };

})(jQuery);
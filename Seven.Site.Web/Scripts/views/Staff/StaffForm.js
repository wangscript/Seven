﻿/*
==============================================================================
//  员工新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.Staff.StaffForm");

    window.views.Staff.StaffForm.initPage = function (form) {

        var _form = $("#" + form);

        var _key = $("#ID", _form).val();

        var _controlInit = function () {
            $("#RealName", _form).validatebox({
                required: true
            });

            $("#Sex", _form).combobox({
                url: "/Staff/SexJson",
                valueField: "Value",
                textField: "Text",
                width: 200
            });

            $("#Remark", _form).attr(window.comlib.textAreaAttr);
        };

        var _bindButtonEvent = function () {
            $("#select_company", _form).click(function () {
                var el = $("#CompanyID", _form);
                window.comlib.showCompanySelector(function (node) {
                    if (node) {
                        el.val(node.id);
                        el.next().text(node.text);
                    }
                }, el.val(), true);
            });

            $("#select_dept", _form).click(function () {
                var elTemp = $("#CompanyID", _form);
                if (elTemp.val() == "0") {
                    $.messager.alert("请先选择公司！"); return;
                }
                else {
                    var el = $("#DeptID", _form);
                    window.comlib.showDeptSelector(function (node) {
                        if (node) {
                            el.val(node.id);
                            el.next().text(node.text);
                        }
                    }, el.val(), true);
                }
            });
        };

        _controlInit();
        _bindButtonEvent();
    };

})(jQuery);
﻿(function ($) {

    $.util.namespace("views.DeskModule.UserDeskModuleManage");

    var layout1 = "#layout", datagrid1 = "#dg-left", datagrid2 = "#dg-right";
    var currentArgument = {};
    window.views.DeskModule.UserDeskModuleManage.initPage = function (permissions, currentUserId, left, right) {
        currentArgument.left = left; currentArgument.right = right; currentArgument.currentUserId = currentUserId;
        var data = window.jeasyui.helper.buildToolbarUsePermissionJson(permissions, true);
        data.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.DeskModule.reloadUserDeskModule(); } } });

        var layoutResize = function () {
            var size = $.util.windowSize();
            var layout = $(layout1);
            var width = parseInt(size.width - 70) / 2;
            //以下对layout的resize方式，在遨游浏览器中无效
            //var west = layout.layout("panel", "west");
            //var east = layout.layout("panel", "east");
            //west.panel("options").width = width;
            //east.panel("options").width = width;
            //layout.layout("resize");

            //以下写法，先设置west、east的宽度，再用js方式去初始化layout，兼容性好
            var west = $("#left", layout).width(width);
            var east = $("#right", layout).width(width);
            layout.layout({ fit: true, border: false });
        };

        var toolBarInit = function () {
            var center1 = $(layout1).layout("panel", "center");
            $.each(data, function (index, t) {
                if (t.type && t.type == "button") {
                    var opts = t.options;
                    opts.plain = true; opts.onClick = opts.onclick;
                    $("<a />").linkbutton(opts).appendTo(center1);
                    $("<br /><br />").appendTo(center1);
                }
            });
        };

        var dblDataGridInit = function () {

            var frozenColumns = function (showCheckBox) {
                var result = [];
                if (showCheckBox) {
                    result.push({ field: 'ck', checkbox: true });
                }
                var normal = [{ field: 'DeskModuleName', title: '名称', width: 100 }];
                $.util.merge(result, normal);
                return result;
            };

            var columns = function (sortableEnable) {
                var result = [];
                var normal = [
                        { field: 'SortNumber', title: '排序号', width: 80 },
                        { field: 'ControlType', title: '控制类型', width: 90 },
                        {
                            field: 'ReceiveMessage', title: '是否接收消息', width: 110,
                            formatter: function (val) {
                                return val ? "<b>接收</b>" : "<b>不接收</b>";
                            },
                            styler: function (val) { return val ? window.comlib.stateColor.green : window.comlib.stateColor.red; }
                        },
                        {
                            field: 'IsShow', title: '是否显示', width: 90,
                            formatter: function (val) {
                                return val ? "<b>显示</b>" : "<b>不显示</b>";
                            },
                            styler: function (val) { return val ? window.comlib.stateColor.green : window.comlib.stateColor.red; }
                        }
                ];
                if (sortableEnable) {
                    $.array.forEach(normal, function (val, index, array) {
                        val.sortable = true;
                    });
                }
                $.util.merge(result, normal);
                return result;
            };

            var options1 = {
                url: "/DeskModule/UserDeskModuleJson",
                queryParams: { userId: currentUserId, location: "left" },
                idField: "ID",
                title: "左边模块",
                rownumbers: true,
                nowrap: true,
                striped: true,
                fit: true,
                border: false,
                remoteSort: false,
                singleSelect: true,
                frozenColumns: [frozenColumns(false)],
                columns: [columns(true)],
                pagination: false,
                onSelect: function (rowIndex, rowData) {
                    $(datagrid2).datagrid('clearSelections');
                }
            };

            var options2 = {
                url: "/DeskModule/UserDeskModuleJson",
                queryParams: { userId: currentUserId, location: "right" },
                idField: "ID",
                title: "右边模块",
                rownumbers: true,
                nowrap: true,
                striped: true,
                fit: true,
                border: false,
                remoteSort: false,
                singleSelect: true,
                frozenColumns: [frozenColumns(false)],
                columns: [columns(true)],
                pagination: false,
                onSelect: function (rowIndex, rowData) {
                    $(datagrid1).datagrid('clearSelections');
                }
            };

            $(datagrid1).datagrid(options1); $(datagrid2).datagrid(options2);
        };

        layoutResize();
        toolBarInit();
        dblDataGridInit();
    };

    window.views.DeskModule.editUserDeskModule = function () {
        var row = $(datagrid1).datagrid("getSelected") || $(datagrid2).datagrid("getSelected");
        if (row) {
            $.easyui.showDialog({
                title: "设置桌面模块 " + row.DeskModuleName,
                href: "/DeskModule/UserDeskModuleEditForm?id=" + row.ID,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.DeskModule.saveUserDeskModule(model);
                    } else { return false; }
                },
                width: 850,
                height: 255
            });
        }
    };

    window.views.DeskModule.saveUserDeskModule = function (model) {
        $.easyui.loading({ locale: window.top.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/DeskModule/SaveUserDeskModule", model, function (result) {
            $.easyui.loaded(window.top.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, function () { window.views.DeskModule.reloadUserDeskModule(model.ID, model.OriginalLocation, model.Location); });
        }, 'json');
    };

    window.views.DeskModule.reloadUserDeskModule = function (id, orignalLocation, location) {
        if (arguments.length > 0 && ((id == "0" && location == currentArgument.left) || (id != "0" && (location == currentArgument.left) && (orignalLocation == currentArgument.left)))) {
            $(datagrid1).datagrid('clearSelections');
            $(datagrid1).datagrid("reload");
        }
        else if (arguments.length > 0 && ((id == "0" && location == currentArgument.right) || (id != "0" && (location == currentArgument.right) && (orignalLocation == currentArgument.right)))) {
            $(datagrid2).datagrid('clearSelections');
            $(datagrid2).datagrid("reload");
        }
        else {
            //一次性刷新
            $.post("/DeskModule/UserDeskModuleJsonForOnce", { userId: currentArgument.currentUserId }, function (data) {
                if (data.left) {
                    $(datagrid1).datagrid('clearSelections');
                    $(datagrid1).datagrid("loadData", data.left);
                }
                if (data.right) {
                    $(datagrid2).datagrid('clearSelections');
                    $(datagrid2).datagrid("loadData", data.right);
                }
            });
        }
    };

})(jQuery);
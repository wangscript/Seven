﻿(function ($) {

    $.util.namespace("views.Menu.MenuPermissionManage");

    var dg = "#dg-function";
    var currentArgument = {};
    window.views.Menu.MenuPermissionManage.initPage = function (menuId, menuName, buttonType, permissions) {
        currentArgument.menuId = menuId; currentArgument.menuName = menuName;
        var data = window.jeasyui.helper.buildToolbarUsePermissionJson(permissions, true);
        data.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.Menu.reloadPermission(); } } });

        var _dataGridInit = function () {
            var toolbar = $("<div />").appendTo("body").toolbar({
                data: data
            }).toolbar("toolbar");

            var columns = function (sortableEnable) {
                var result = [];
                var normal = [
                    {
                        field: 'Name', title: '权限名称', width: 150,
                        formatter: function (val, row) {
                            var str = "<b>" + val + "</b>" + (row.multiple ? ("&nbsp;" + window.comlib.icon("icon-link")) : "");
                            str += "&nbsp;" + (row.funcc ? ("&nbsp;" + window.comlib.icon("icon-func")) : "");
                            return str;
                        }
                    },
                    { field: 'IconCls', title: '图标', width: 80, formatter: function (val) { return window.comlib.icon(val); } },
                    { field: 'SortNumber', title: '排序号及分组', width: 120, formatter: function (val, row) { return $.string.isNullOrWhiteSpace(row.GroupMark) ? val : val + "&nbsp;&nbsp;" + row.GroupMark; } },
                    {
                        field: 'Type', title: '类型', width: 80,
                        formatter: function (val, row) { return val == buttonType ? "<b>" + val + "</b>" : val; },
                        styler: function (val, row) {
                            if (val == buttonType)
                            { return window.comlib.stateColor.green; }
                        }
                    },
                    {
                        field: 'IsShow', title: '是否显示', width: 80,
                        formatter: function (value) {
                            return value ? "<b>显示</b>" : "<b>不显示</b>"
                        },
                        styler: function (val, row) {
                            if (val)
                            { return window.comlib.stateColor.green; }
                            else
                            { return window.comlib.stateColor.red; }
                        }
                    },
                ];
                if (sortableEnable) {
                    $.array.forEach(normal, function (val, index, array) {
                        $.extend(val, { sortable: true });
                    });
                }
                $.util.merge(result, normal);
                return result;
            };

            var options = {
                url: "/Menu/MenuPermissionJson",
                queryParams: { menuId: menuId },
                idField: "ID",
                toolbar: toolbar,
                rownumbers: false,
                fit: true,
                border: false,
                singleSelect: true,
                columns: [columns(false)]
            };

            $(dg).datagrid(options);
        };

        _dataGridInit();
    };

    window.views.Menu.createPermission = function () {
        $.easyui.showDialog({
            title: "新建权限",
            href: "/Menu/MenuPermissionCreateForm?menuId=" + currentArgument.menuId + "&menuName=" + escape(currentArgument.menuName),
            onSave: function (dialog) {
                var model = window.jeasyui.helper.formValidate(dialog);
                if (model != null) {
                    window.views.Menu.savePermission(model);
                } else { return false; }
            },
            width: 850,
            height: 360,
            topMost: true
        });
    };

    window.views.Menu.editPermission = function (row) {
        row = row == undefined ? $(dg).datagrid("getSelected") : row;
        if (row) {
            $.easyui.showDialog({
                title: "编辑权限-" + row.Name,
                href: "/Menu/MenuPermissionEditForm?id=" + row.ID + "&menuName=" + escape(currentArgument.menuName),
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.Menu.savePermission(model);
                    } else { return false; }
                },
                width: 850,
                height: 360,
                topMost: true
            });
        }
    };

    window.views.Menu.savePermission = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/Menu/SaveMenuPermission", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Menu.reloadPermission);
        }, 'json');
    };

    window.views.Menu.removePermission = function (row) {
        row = row == undefined ? $(dg).datagrid("getSelected") : row;
        if (row) {
            var _callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/Menu/RemoveMenuPermission", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Menu.reloadPermission);
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.Name + " 吗?", _callback);
        }
    };

    window.views.Menu.reloadPermission = function () {
        $(dg).datagrid('clearSelections');
        //$(dg).datagrid('clearChecked');
        $(dg).datagrid("load");
    };

})(jQuery);